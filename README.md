# SemanaBecario-WebService

Repositorio que contiene, todo lo relacionado al "Servicio web" para el proyecto "Semana"

## Instalar proyecto 

1.  sudo chmod -R 755 storage
2.  composer install
3.  cp .env.example .env
4.  php artisan key:generate

## Instalar librerías

*  [FCM](https://github.com/brozot/Laravel-FCM)
*  [Hashids](https://blog.xoborg.com/integrar-hashids-en-laravel-5-3-7d5e99e69cfe)