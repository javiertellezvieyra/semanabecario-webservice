<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActividadesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('actividades', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombreActividad');
            $table->text('descripcion');
            $table->string('aula', 45);
            $table->integer('cupoMaximo')->default('-1');
            $table->integer('cupoActual')->default('0');
            $table->integer('idSede');
            $table->integer('idTipo');
            $table->integer('minutosTotales');
            $table->string('nombreImagen')->default('0');
            $table->string('linkCuestionario');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('actividades');
    }
}
