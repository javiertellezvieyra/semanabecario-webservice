<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class NotificacionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //

        Schema::dropIfExists('notificaciones');
        Schema::create('notificaciones', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('idAsistente')->unsigned();
            $table->foreign('idAsistente')->references('id')->on('asistentes');
            $table->string('titulo');
            $table->string('texto');
            $table->string('accion');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notificaciones');
    }
}
