<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAsistenteHasActividad extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('asistente_has_actividad', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('idAsistente');
            $table->integer('idActividad');
            $table->integer('contadorMinutos')->default('0');
            $table->string('tokenAsistente');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('asistente_has_actividad');
    }
}
