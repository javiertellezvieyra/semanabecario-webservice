<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class FechasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('fechas')->insert([
            'fecha' => Carbon::now()->addDays(20),
            'idActividad' => 2,
            'duracion'=>90
        ]);
        DB::table('fechas')->insert([
            'fecha' => Carbon::now()->addDays(21),
            'idActividad' => 2,
            'duracion'=>90,

        ]);
        DB::table('fechas')->insert([
            'fecha' => Carbon::now()->addDays(22),
            'idActividad' => 2,
            'duracion'=>90,
        ]);

        DB::table('fechas')->insert([
            'fecha' => Carbon::now()->addDays(19),
            'idActividad' => 1,
            'duracion'=>120,
        ]);
        DB::table('fechas')->insert([
            'fecha' => Carbon::now()->addDays(20),
            'idActividad' => 1,
            'duracion'=>120,
        ]);

        DB::table('fechas')->insert([
            'fecha' => Carbon::now()->addDays(23),
            'idActividad' => 6,
            'duracion'=>120,
        ]);

        DB::table('fechas')->insert([
            'fecha' => Carbon::now()->addDays(24),
            'idActividad' => 3,
            'duracion'=>90,
        ]);

        DB::table('fechas')->insert([
            'fecha' => Carbon::now()->addDays(20),
            'idActividad' => 5,
            'duracion'=>120,
        ]);

        DB::table('fechas')->insert([
            'fecha' => Carbon::now()->addDays(18),
            'idActividad' => 4,
            'duracion'=>2,
        ]);
    }
}
