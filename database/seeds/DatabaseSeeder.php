<?php

use Illuminate\Database\Seeder;
use App\Ponente;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
    	$this->call(TiposTableSeeder::class);
    	$this->call(SedesTableSeeder::class);
        $this->call(ActividadesTableSeeder::class);
        $this->call(FechasTableSeeder::class);
        $this->call(PonenteTableSeeder::class);
        $this->call(ActividadPonenteTableSeeder::class);
        $this->call(LineaBecasTableSeeder::class);
        $this->call(FacultadesTableSeeder::class);
        $this->call(CarreraFacultadTableSeeder::class);
        $this->call(UsersTableSeeder::class);
    }
}
