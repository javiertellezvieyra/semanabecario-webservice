<?php

use Illuminate\Database\Seeder;

class CarreraFacultadTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	// carreras_f1
    	DB::table('carrera_facultad')->insert([
            'idFacultad' => 1,
            'idCarrera' => 1,
            'nombreCarrera' => "Arquitectura",
        ]);
        DB::table('carrera_facultad')->insert([
            'idFacultad' => 1,
            'idCarrera' => 2,
            'nombreCarrera' => "Arquitectura del paisaje",
        ]);
        DB::table('carrera_facultad')->insert([
            'idFacultad' => 1,
            'idCarrera' => 2,
            'nombreCarrera' => "Diseño industrial",
        ]);
        DB::table('carrera_facultad')->insert([
            'idFacultad' => 1,
            'idCarrera' => 2,
            'nombreCarrera' => "Urbanismo",
        ]);
        // carreras_f2
    	DB::table('carrera_facultad')->insert([
            'idFacultad' => 2,
            'idCarrera' => 1,
            'nombreCarrera' => "Artes visuales",
        ]);
        DB::table('carrera_facultad')->insert([
            'idFacultad' => 2,
            'idCarrera' => 2,
            'nombreCarrera' => "Diseño y comunicación visual",
        ]);
        DB::table('carrera_facultad')->insert([
            'idFacultad' => 2,
            'idCarrera' => 3,
            'nombreCarrera' => "Arte y  diseño",
        ]);
        // carreras_f3
    	DB::table('carrera_facultad')->insert([
            'idFacultad' => 3,
            'idCarrera' => 1,
            'nombreCarrera' => "Actuaría",
        ]);
        DB::table('carrera_facultad')->insert([
            'idFacultad' => 3,
            'idCarrera' => 2,
            'nombreCarrera' => "Biología",
        ]);
        DB::table('carrera_facultad')->insert([
            'idFacultad' => 3,
            'idCarrera' => 3,
            'nombreCarrera' => "Ciencias de la Computación",
        ]);
        DB::table('carrera_facultad')->insert([
            'idFacultad' => 3,
            'idCarrera' => 4,
            'nombreCarrera' => "Ciencias de la Tierra",
        ]);
        DB::table('carrera_facultad')->insert([
            'idFacultad' => 3,
            'idCarrera' => 5,
            'nombreCarrera' => "Física",
        ]);
        DB::table('carrera_facultad')->insert([
            'idFacultad' => 3,
            'idCarrera' => 6,
            'nombreCarrera' => "Física Biomédica",
        ]);
        DB::table('carrera_facultad')->insert([
            'idFacultad' => 3,
            'idCarrera' => 7,
            'nombreCarrera' => "Matemáticas",
        ]);
        DB::table('carrera_facultad')->insert([
            'idFacultad' => 3,
            'idCarrera' => 8,
            'nombreCarrera' => "Matemáticas Aplicadas",
        ]);
        // carreras_f4
    	DB::table('carrera_facultad')->insert([
            'idFacultad' => 4,
            'idCarrera' => 1,
            'nombreCarrera' => "Antropología",
        ]);
        DB::table('carrera_facultad')->insert([
            'idFacultad' => 4,
            'idCarrera' => 2,
            'nombreCarrera' => "Ciencias de la Comunicación",
        ]);
        DB::table('carrera_facultad')->insert([
            'idFacultad' => 4,
            'idCarrera' => 3,
            'nombreCarrera' => "Ciencias Políticas y Administración Pública",
        ]);
        DB::table('carrera_facultad')->insert([
            'idFacultad' => 4,
            'idCarrera' => 4,
            'nombreCarrera' => "Relaciones Internacionales",
        ]);
        DB::table('carrera_facultad')->insert([
            'idFacultad' => 4,
            'idCarrera' => 5,
            'nombreCarrera' => "Sociología",
        ]);
        // carreras_f5
    	DB::table('carrera_facultad')->insert([
            'idFacultad' => 5,
            'idCarrera' => 1,
            'nombreCarrera' => "Ingeniería Química",
        ]);
        DB::table('carrera_facultad')->insert([
            'idFacultad' => 5,
            'idCarrera' => 2,
            'nombreCarrera' => "Ingeniería Química Metalúrgica",
        ]);
        DB::table('carrera_facultad')->insert([
            'idFacultad' => 5,
            'idCarrera' => 3,
            'nombreCarrera' => "Química",
        ]);
        DB::table('carrera_facultad')->insert([
            'idFacultad' => 5,
            'idCarrera' => 4,
            'nombreCarrera' => "Química de Alimentos",
        ]);
        DB::table('carrera_facultad')->insert([
            'idFacultad' => 5,
            'idCarrera' => 5,
            'nombreCarrera' => "Química Farmacéutico Biológica",
        ]);
        // carreras_f6
    	DB::table('carrera_facultad')->insert([
            'idFacultad' => 6,
            'idCarrera' => 1,
            'nombreCarrera' => "Administración",
        ]);
        DB::table('carrera_facultad')->insert([
            'idFacultad' => 6,
            'idCarrera' => 2,
            'nombreCarrera' => "Contaduría",
        ]);
        DB::table('carrera_facultad')->insert([
            'idFacultad' => 6,
            'idCarrera' => 3,
            'nombreCarrera' => "Informática",
        ]);
        DB::table('carrera_facultad')->insert([
            'idFacultad' => 6,
            'idCarrera' => 4,
            'nombreCarrera' => "Negocios Internacionales",
        ]);
        // carreras_f7
    	DB::table('carrera_facultad')->insert([
            'idFacultad' => 7,
            'idCarrera' => 1,
            'nombreCarrera' => "Derecho",
        ]);
        // carreras_f8
    	DB::table('carrera_facultad')->insert([
            'idFacultad' => 8,
            'idCarrera' => 1,
            'nombreCarrera' => "Economía",
        ]);
        // carreras_f9
    	DB::table('carrera_facultad')->insert([
            'idFacultad' => 9,
            'idCarrera' => 1,
            'nombreCarrera' => "Enfermería",
        ]);
        DB::table('carrera_facultad')->insert([
            'idFacultad' => 9,
            'idCarrera' => 1,
            'nombreCarrera' => "Enfermería y Obstetricia",
        ]);
        // carreras_f10
    	DB::table('carrera_facultad')->insert([
            'idFacultad' => 10,
            'idCarrera' => 1,
            'nombreCarrera' => "Ecología",
        ]);
        // carreras_f11
    	DB::table('carrera_facultad')->insert([
            'idFacultad' => 11,
            'idCarrera' => 1,
            'nombreCarrera' => "Administración de Archivos y Gestión Documental",
        ]);
        DB::table('carrera_facultad')->insert([
            'idFacultad' => 11,
            'idCarrera' => 2,
            'nombreCarrera' => "Bibliotecología y Estudios de la Información",
        ]);
        DB::table('carrera_facultad')->insert([
            'idFacultad' => 11,
            'idCarrera' => 3,
            'nombreCarrera' => "Desarrollo y Gestión Interculturales",
        ]);
        DB::table('carrera_facultad')->insert([
            'idFacultad' => 11,
            'idCarrera' => 4,
            'nombreCarrera' => "Estudios Latinoamericanos",
        ]);
        DB::table('carrera_facultad')->insert([
            'idFacultad' => 11,
            'idCarrera' => 5,
            'nombreCarrera' => "Filosofía",
        ]);
        DB::table('carrera_facultad')->insert([
            'idFacultad' => 11,
            'idCarrera' => 6,
            'nombreCarrera' => "Geografía",
        ]);
        DB::table('carrera_facultad')->insert([
            'idFacultad' => 11,
            'idCarrera' => 7,
            'nombreCarrera' => "Historia",
        ]);
        DB::table('carrera_facultad')->insert([
            'idFacultad' => 11,
            'idCarrera' => 8,
            'nombreCarrera' => "Lengua y Literaturas Hispánicas",
        ]);
        DB::table('carrera_facultad')->insert([
            'idFacultad' => 11,
            'idCarrera' => 9,
            'nombreCarrera' => "Lengua y Literaturas Modernas Alemanas",
        ]);
        DB::table('carrera_facultad')->insert([
            'idFacultad' => 11,
            'idCarrera' => 10,
            'nombreCarrera' => "Lengua y Literaturas Modernas Francesas",
        ]);
        DB::table('carrera_facultad')->insert([
            'idFacultad' => 11,
            'idCarrera' => 11,
            'nombreCarrera' => "Lengua y Literaturas Modernas Inglesas",
        ]);
        DB::table('carrera_facultad')->insert([
            'idFacultad' => 11,
            'idCarrera' => 12,
            'nombreCarrera' => "Lengua y Literaturas Modernas Italianas",
        ]);
        DB::table('carrera_facultad')->insert([
            'idFacultad' => 11,
            'idCarrera' => 13,
            'nombreCarrera' => "Lengua y Literaturas Modernas Portuguesas",
        ]);
        DB::table('carrera_facultad')->insert([
            'idFacultad' => 11,
            'idCarrera' => 14,
            'nombreCarrera' => "Letras Clásicas",
        ]);
        DB::table('carrera_facultad')->insert([
            'idFacultad' => 11,
            'idCarrera' => 15,
            'nombreCarrera' => "Literatura Dramática y Teatro",
        ]);
        DB::table('carrera_facultad')->insert([
            'idFacultad' => 11,
            'idCarrera' => 16,
            'nombreCarrera' => "Pedagogía",
        ]);
        // carreras_f12
    	DB::table('carrera_facultad')->insert([
            'idFacultad' => 12,
            'idCarrera' => 1,
            'nombreCarrera' => "Ingeniería Ambiental",
        ]);
        DB::table('carrera_facultad')->insert([
            'idFacultad' => 12,
            'idCarrera' => 2,
            'nombreCarrera' => "Ingeniería Civil",
        ]);
        DB::table('carrera_facultad')->insert([
            'idFacultad' => 12,
            'idCarrera' => 3,
            'nombreCarrera' => "Ingeniería de Minas y Metalurgia",
        ]);
        DB::table('carrera_facultad')->insert([
            'idFacultad' => 12,
            'idCarrera' => 4,
            'nombreCarrera' => "Ingeniería Eléctrica Electrónica",
        ]);
        DB::table('carrera_facultad')->insert([
            'idFacultad' => 12,
            'idCarrera' => 5,
            'nombreCarrera' => "Ingeniería en Computación",
        ]);
        DB::table('carrera_facultad')->insert([
            'idFacultad' => 12,
            'idCarrera' => 6,
            'nombreCarrera' => "Ingeniería en Telecomunicaciones",
        ]);
        DB::table('carrera_facultad')->insert([
            'idFacultad' => 12,
            'idCarrera' => 7,
            'nombreCarrera' => "Ingeniería Geofísica",
        ]);
        DB::table('carrera_facultad')->insert([
            'idFacultad' => 12,
            'idCarrera' => 8,
            'nombreCarrera' => "Ingeniería Geológica",
        ]);
        DB::table('carrera_facultad')->insert([
            'idFacultad' => 12,
            'idCarrera' => 9,
            'nombreCarrera' => "Ingeniería Geomática",
        ]);
        DB::table('carrera_facultad')->insert([
            'idFacultad' => 12,
            'idCarrera' => 10,
            'nombreCarrera' => "Ingeniería Industrial",
        ]);
        DB::table('carrera_facultad')->insert([
            'idFacultad' => 12,
            'idCarrera' => 11,
            'nombreCarrera' => "Ingeniería Mecánica",
        ]);
        DB::table('carrera_facultad')->insert([
            'idFacultad' => 12,
            'idCarrera' => 12,
            'nombreCarrera' => "Ingeniería Mecatrónica",
        ]);
        DB::table('carrera_facultad')->insert([
            'idFacultad' => 12,
            'idCarrera' => 13,
            'nombreCarrera' => "Ingeniería Petrolera",
        ]);
        DB::table('carrera_facultad')->insert([
            'idFacultad' => 12,
            'idCarrera' => 14,
            'nombreCarrera' => "Ingeniería en Sistemas Biomédicos",
        ]);
        // carreras_f13
    	DB::table('carrera_facultad')->insert([
            'idFacultad' => 13,
            'idCarrera' => 1,
            'nombreCarrera' => "Ciencia Forense",
        ]);
        DB::table('carrera_facultad')->insert([
            'idFacultad' => 13,
            'idCarrera' => 2,
            'nombreCarrera' => "Fisioterapia",
        ]);
        DB::table('carrera_facultad')->insert([
            'idFacultad' => 13,
            'idCarrera' => 3,
            'nombreCarrera' => "Investigación Biomédica Básica",
        ]);
        DB::table('carrera_facultad')->insert([
            'idFacultad' => 13,
            'idCarrera' => 4,
            'nombreCarrera' => "Médico Cirujano",
        ]);
        DB::table('carrera_facultad')->insert([
            'idFacultad' => 13,
            'idCarrera' => 5,
            'nombreCarrera' => "Neurociencias",
        ]);
        // carreras_f14
    	DB::table('carrera_facultad')->insert([
            'idFacultad' => 14,
            'idCarrera' => 1,
            'nombreCarrera' => "Educación Musical",
        ]);
        DB::table('carrera_facultad')->insert([
            'idFacultad' => 14,
            'idCarrera' => 2,
            'nombreCarrera' => "Etnomusicología",
        ]);
        DB::table('carrera_facultad')->insert([
            'idFacultad' => 14,
            'idCarrera' => 3,
            'nombreCarrera' => "Canto",
        ]);
        DB::table('carrera_facultad')->insert([
            'idFacultad' => 14,
            'idCarrera' => 4,
            'nombreCarrera' => "Composición",
        ]);
        DB::table('carrera_facultad')->insert([
            'idFacultad' => 14,
            'idCarrera' => 5,
            'nombreCarrera' => "Instrumentista",
        ]);
        DB::table('carrera_facultad')->insert([
            'idFacultad' => 14,
            'idCarrera' => 6,
            'nombreCarrera' => "Piano",
        ]);
        DB::table('carrera_facultad')->insert([
            'idFacultad' => 14,
            'idCarrera' => 7,
            'nombreCarrera' => "Teatro y Actuación",
        ]);
        // carreras_f15
    	DB::table('carrera_facultad')->insert([
            'idFacultad' => 15,
            'idCarrera' => 1,
            'nombreCarrera' => "Cirujano Dentista",
        ]);
        // carreras_f16
    	DB::table('carrera_facultad')->insert([
            'idFacultad' => 16,
            'idCarrera' => 1,
            'nombreCarrera' => "Trabajo Social",
        ]);
        // carreras_f17
    	DB::table('carrera_facultad')->insert([
            'idFacultad' => 17,
            'idCarrera' => 1,
            'nombreCarrera' => "Medicina Veterinaria y Zootecnia",
        ]);
        // carreras_f18
    	DB::table('carrera_facultad')->insert([
            'idFacultad' => 18,
            'idCarrera' => 1,
            'nombreCarrera' => "Psicología",
        ]);
        // carreras_f19
    	DB::table('carrera_facultad')->insert([
            'idFacultad' => 19,
            'idCarrera' => 1,
            'nombreCarrera' => "Lingüistica Aplicada",
        ]);
    	DB::table('carrera_facultad')->insert([
            'idFacultad' => 19,
            'idCarrera' => 1,
            'nombreCarrera' => "Traducción",
        ]);
        // carreras_f20
    	DB::table('carrera_facultad')->insert([
            'idFacultad' => 20,
            'idCarrera' => 1,
            'nombreCarrera' => "Arte y Diseño",
        ]);
        DB::table('carrera_facultad')->insert([
            'idFacultad' => 20,
            'idCarrera' => 1,
            'nombreCarrera' => "Artes Visuales",
        ]);
        // carreras_f21
    	DB::table('carrera_facultad')->insert([
            'idFacultad' => 21,
            'idCarrera' => 1,
            'nombreCarrera' => "Ciencias Genómicas",
        ]);
    	// carreras_f22
    	DB::table('carrera_facultad')->insert([
            'idFacultad' => 22,
            'idCarrera' => 1,
            'nombreCarrera' => "Ingeniería en Energías Renovables",
        ]);
        // carreras_f23
    	DB::table('carrera_facultad')->insert([
            'idFacultad' => 23,
            'idCarrera' => 1,
            'nombreCarrera' => "Nanotecnología",
        ]);
        // carreras_f24
    	DB::table('carrera_facultad')->insert([
            'idFacultad' => 24,
            'idCarrera' => 1,
            'nombreCarrera' => "Ing. en Telecomunicaciones, Sistemas y Electrónica",
        ]);
        DB::table('carrera_facultad')->insert([
            'idFacultad' => 24,
            'idCarrera' => 2,
            'nombreCarrera' => "Ing. Industrial",
        ]);
        DB::table('carrera_facultad')->insert([
            'idFacultad' => 24,
            'idCarrera' => 3,
            'nombreCarrera' => "Ing. Mecánica Eléctrica",
        ]);
        DB::table('carrera_facultad')->insert([
            'idFacultad' => 24,
            'idCarrera' => 4,
            'nombreCarrera' => "Ing. Química",
        ]);
        DB::table('carrera_facultad')->insert([
            'idFacultad' => 24,
            'idCarrera' => 5,
            'nombreCarrera' => "Tecnología",
        ]);
        DB::table('carrera_facultad')->insert([
            'idFacultad' => 24,
            'idCarrera' => 6,
            'nombreCarrera' => "Bioquímica Diagnóstica",
        ]);
        DB::table('carrera_facultad')->insert([
            'idFacultad' => 24,
            'idCarrera' => 7,
            'nombreCarrera' => "Farmacia",
        ]);
        DB::table('carrera_facultad')->insert([
            'idFacultad' => 24,
            'idCarrera' => 8,
            'nombreCarrera' => "Ing. Agrícola",
        ]);
        DB::table('carrera_facultad')->insert([
            'idFacultad' => 24,
            'idCarrera' => 9,
            'nombreCarrera' => "Ing. en Alimentos",
        ]);
        DB::table('carrera_facultad')->insert([
            'idFacultad' => 24,
            'idCarrera' => 10,
            'nombreCarrera' => "Medicina Veterinaria y Zootecnia",
        ]);
        DB::table('carrera_facultad')->insert([
            'idFacultad' => 24,
            'idCarrera' => 11,
            'nombreCarrera' => "Química",
        ]);
        DB::table('carrera_facultad')->insert([
            'idFacultad' => 24,
            'idCarrera' => 12,
            'nombreCarrera' => "Química Industrial",
        ]);
        DB::table('carrera_facultad')->insert([
            'idFacultad' => 24,
            'idCarrera' => 13,
            'nombreCarrera' => "Administración",
        ]);
        DB::table('carrera_facultad')->insert([
            'idFacultad' => 24,
            'idCarrera' => 14,
            'nombreCarrera' => "Contaduría",
        ]);
        DB::table('carrera_facultad')->insert([
            'idFacultad' => 24,
            'idCarrera' => 15,
            'nombreCarrera' => "Informática",
        ]);
        DB::table('carrera_facultad')->insert([
            'idFacultad' => 24,
            'idCarrera' => 16,
            'nombreCarrera' => "Diseño y Comunicación Visual",
        ]);
        // carreras_f25
    	DB::table('carrera_facultad')->insert([
            'idFacultad' => 25,
            'idCarrera' => 1,
            'nombreCarrera' => "Actuaría",
        ]);
        DB::table('carrera_facultad')->insert([
            'idFacultad' => 25,
            'idCarrera' => 2,
            'nombreCarrera' => "Arquitectura",
        ]);
        DB::table('carrera_facultad')->insert([
            'idFacultad' => 25,
            'idCarrera' => 3,
            'nombreCarrera' => "Ing. Civil",
        ]);
        DB::table('carrera_facultad')->insert([
            'idFacultad' => 25,
            'idCarrera' => 4,
            'nombreCarrera' => "Matemáticas Aplicadas y Computación",
        ]);
        DB::table('carrera_facultad')->insert([
            'idFacultad' => 25,
            'idCarrera' => 5,
            'nombreCarrera' => "Ciencias Políticas y Administración Pública",
        ]);
        DB::table('carrera_facultad')->insert([
            'idFacultad' => 25,
            'idCarrera' => 6,
            'nombreCarrera' => "Comunicación",
        ]);
        DB::table('carrera_facultad')->insert([
            'idFacultad' => 25,
            'idCarrera' => 7,
            'nombreCarrera' => "Derecho",
        ]);
        DB::table('carrera_facultad')->insert([
            'idFacultad' => 25,
            'idCarrera' => 8,
            'nombreCarrera' => "Economía",
        ]);
        DB::table('carrera_facultad')->insert([
            'idFacultad' => 25,
            'idCarrera' => 9,
            'nombreCarrera' => "Relaciones Internacionales",
        ]);
        DB::table('carrera_facultad')->insert([
            'idFacultad' => 25,
            'idCarrera' => 10,
            'nombreCarrera' => "Sociología",
        ]);
        DB::table('carrera_facultad')->insert([
            'idFacultad' => 25,
            'idCarrera' => 11,
            'nombreCarrera' => "Diseño Gráfico",
        ]);
        DB::table('carrera_facultad')->insert([
            'idFacultad' => 25,
            'idCarrera' => 12,
            'nombreCarrera' => "Enseñanza de Inglés",
        ]);
        DB::table('carrera_facultad')->insert([
            'idFacultad' => 25,
            'idCarrera' => 13,
            'nombreCarrera' => "Enseñanza de (Alemán; Español; Francés; Inglés o Italiano) como Lengua Extranjera",
        ]);
        DB::table('carrera_facultad')->insert([
            'idFacultad' => 25,
            'idCarrera' => 14,
            'nombreCarrera' => "Filosofía",
        ]);
        DB::table('carrera_facultad')->insert([
            'idFacultad' => 25,
            'idCarrera' => 15,
            'nombreCarrera' => "Historia",
        ]);
        DB::table('carrera_facultad')->insert([
            'idFacultad' => 25,
            'idCarrera' => 16,
            'nombreCarrera' => "Lengua y Literaturas Hispánicas",
        ]);
        DB::table('carrera_facultad')->insert([
            'idFacultad' => 25,
            'idCarrera' => 17,
            'nombreCarrera' => "Pedagogía",
        ]);
        // carreras_f26
    	DB::table('carrera_facultad')->insert([
            'idFacultad' => 26,
            'idCarrera' => 1,
            'nombreCarrera' => "Biología",
        ]);
        DB::table('carrera_facultad')->insert([
            'idFacultad' => 26,
            'idCarrera' => 2,
            'nombreCarrera' => "Cirujano Dentista",
        ]);
        DB::table('carrera_facultad')->insert([
            'idFacultad' => 26,
            'idCarrera' => 3,
            'nombreCarrera' => "Enfermería",
        ]);
        DB::table('carrera_facultad')->insert([
            'idFacultad' => 26,
            'idCarrera' => 4,
            'nombreCarrera' => "Médico Cirujano",
        ]);
        DB::table('carrera_facultad')->insert([
            'idFacultad' => 26,
            'idCarrera' => 5,
            'nombreCarrera' => "Optometría",
        ]);
        DB::table('carrera_facultad')->insert([
            'idFacultad' => 26,
            'idCarrera' => 6,
            'nombreCarrera' => "Psicología",
        ]);
        // carreras_f27
    	DB::table('carrera_facultad')->insert([
            'idFacultad' => 27,
            'idCarrera' => 1,
            'nombreCarrera' => "Optometría",
        ]);
        // carreras_f28
    	DB::table('carrera_facultad')->insert([
            'idFacultad' => 28,
            'idCarrera' => 1,
            'nombreCarrera' => "Arquitectura",
        ]);
        DB::table('carrera_facultad')->insert([
            'idFacultad' => 28,
            'idCarrera' => 2,
            'nombreCarrera' => "Diseño Industrial",
        ]);
        DB::table('carrera_facultad')->insert([
            'idFacultad' => 28,
            'idCarrera' => 3,
            'nombreCarrera' => "Ing. Civil",
        ]);
        DB::table('carrera_facultad')->insert([
            'idFacultad' => 28,
            'idCarrera' => 4,
            'nombreCarrera' => "Ing. Eléctrica y Electrónica",
        ]);
        DB::table('carrera_facultad')->insert([
            'idFacultad' => 28,
            'idCarrera' => 5,
            'nombreCarrera' => "Ing. Computación",
        ]);
        DB::table('carrera_facultad')->insert([
            'idFacultad' => 28,
            'idCarrera' => 6,
            'nombreCarrera' => "Ing. Industrial",
        ]);
        DB::table('carrera_facultad')->insert([
            'idFacultad' => 28,
            'idCarrera' => 7,
            'nombreCarrera' => "Ing. Mecánica",
        ]);
        DB::table('carrera_facultad')->insert([
            'idFacultad' => 28,
            'idCarrera' => 8,
            'nombreCarrera' => "Comunicación y Periodismo",
        ]);
        DB::table('carrera_facultad')->insert([
            'idFacultad' => 28,
            'idCarrera' => 9,
            'nombreCarrera' => "Derecho",
        ]);
        DB::table('carrera_facultad')->insert([
            'idFacultad' => 28,
            'idCarrera' => 10,
            'nombreCarrera' => "Economía",
        ]);
        DB::table('carrera_facultad')->insert([
            'idFacultad' => 28,
            'idCarrera' => 11,
            'nombreCarrera' => "Planificación para el Desarrollo Agropecuario",
        ]);
        DB::table('carrera_facultad')->insert([
            'idFacultad' => 28,
            'idCarrera' => 12,
            'nombreCarrera' => "Relaciones Internacionales",
        ]);
        DB::table('carrera_facultad')->insert([
            'idFacultad' => 28,
            'idCarrera' => 13,
            'nombreCarrera' => "Sociología",
        ]);
        DB::table('carrera_facultad')->insert([
            'idFacultad' => 28,
            'idCarrera' => 14,
            'nombreCarrera' => "Pedagogía",
        ]);
        // carreras_f29
    	DB::table('carrera_facultad')->insert([
            'idFacultad' => 29,
            'idCarrera' => 1,
            'nombreCarrera' => "Ing. Química",
        ]);
        DB::table('carrera_facultad')->insert([
            'idFacultad' => 29,
            'idCarrera' => 2,
            'nombreCarrera' => "Biología",
        ]);
        DB::table('carrera_facultad')->insert([
            'idFacultad' => 29,
            'idCarrera' => 3,
            'nombreCarrera' => "Cirujano Dentista",
        ]);
        DB::table('carrera_facultad')->insert([
            'idFacultad' => 29,
            'idCarrera' => 4,
            'nombreCarrera' => "Enfermería",
        ]);
        DB::table('carrera_facultad')->insert([
            'idFacultad' => 29,
            'idCarrera' => 5,
            'nombreCarrera' => "Médico Cirujano",
        ]);
        DB::table('carrera_facultad')->insert([
            'idFacultad' => 29,
            'idCarrera' => 6,
            'nombreCarrera' => "Nutriología",
        ]);
        DB::table('carrera_facultad')->insert([
            'idFacultad' => 29,
            'idCarrera' => 7,
            'nombreCarrera' => "Psicología",
        ]);
        DB::table('carrera_facultad')->insert([
            'idFacultad' => 29,
            'idCarrera' => 8,
            'nombreCarrera' => "Química Farmacéutico Biológica",
        ]);
        // carreras_f30
    	DB::table('carrera_facultad')->insert([
            'idFacultad' => 30,
            'idCarrera' => 1,
            'nombreCarrera' => "Enfermería",
        ]);
        DB::table('carrera_facultad')->insert([
            'idFacultad' => 30,
            'idCarrera' => 2,
            'nombreCarrera' => "Desarrollo Comunitario para el Envejecimiento",
        ]);
        // carreras_f31
    	DB::table('carrera_facultad')->insert([
            'idFacultad' => 31,
            'idCarrera' => 1,
            'nombreCarrera' => "Ciencias de la Tierra",
        ]);
        DB::table('carrera_facultad')->insert([
            'idFacultad' => 31,
            'idCarrera' => 2,
            'nombreCarrera' => "Ciencias Ambientales",
        ]);
        DB::table('carrera_facultad')->insert([
            'idFacultad' => 31,
            'idCarrera' => 3,
            'nombreCarrera' => "Manejo Sustentable de Zonas Costeras",
        ]);
        DB::table('carrera_facultad')->insert([
            'idFacultad' => 31,
            'idCarrera' => 4,
            'nombreCarrera' => "Geografía Aplicada",
        ]);
        DB::table('carrera_facultad')->insert([
            'idFacultad' => 31,
            'idCarrera' => 5,
            'nombreCarrera' => "Desarrollo y Gestión Interculturales",
        ]);
        // carreras_f32
    	DB::table('carrera_facultad')->insert([
            'idFacultad' => 32,
            'idCarrera' => 1,
            'nombreCarrera' => "Ciencias de la Tierra",
        ]);
        DB::table('carrera_facultad')->insert([
            'idFacultad' => 32,
            'idCarrera' => 2,
            'nombreCarrera' => "Ing. en Energías Renovables",
        ]);
        DB::table('carrera_facultad')->insert([
            'idFacultad' => 32,
            'idCarrera' => 3,
            'nombreCarrera' => "Ortesis y Protesis",
        ]);
        DB::table('carrera_facultad')->insert([
            'idFacultad' => 32,
            'idCarrera' => 4,
            'nombreCarrera' => "Tecnología",
        ]);
        DB::table('carrera_facultad')->insert([
            'idFacultad' => 32,
            'idCarrera' => 5,
            'nombreCarrera' => "Ciencias Genómicas",
        ]);
        DB::table('carrera_facultad')->insert([
            'idFacultad' => 32,
            'idCarrera' => 6,
            'nombreCarrera' => "Neurociencias",
        ]);
        DB::table('carrera_facultad')->insert([
            'idFacultad' => 32,
            'idCarrera' => 7,
            'nombreCarrera' => "Negocios Internacionales",
        ]);
    }
}
