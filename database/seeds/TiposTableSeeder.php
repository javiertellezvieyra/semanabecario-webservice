<?php

use Illuminate\Database\Seeder;

class TiposTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tipos')->insert([
            'nombreTipo' => "Conferencia",
        ]);
        DB::table('tipos')->insert([
            'nombreTipo' => "Taller",
        ]);
        DB::table('tipos')->insert([
            'nombreTipo' => "Mesa Redonda",
        ]);
    }
}
