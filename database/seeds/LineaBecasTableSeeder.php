<?php

use Illuminate\Database\Seeder;

class LineaBecasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('lineas_becas')->insert([
            'nombreLineaBecas' => "Telecomunicaciones",
        ]);
        DB::table('lineas_becas')->insert([
            'nombreLineaBecas' => "Seguridad informática",
        ]);
        DB::table('lineas_becas')->insert([
            'nombreLineaBecas' => "Tecnologías para la Educación",
        ]);
        DB::table('lineas_becas')->insert([
            'nombreLineaBecas' => "Ingeniería de Software",
        ]);
        DB::table('lineas_becas')->insert([
            'nombreLineaBecas' => "Realidad Virtual",
        ]);
        DB::table('lineas_becas')->insert([
            'nombreLineaBecas' => "Desarrollo y evaluación de accesibilidad Web",
        ]);
        DB::table('lineas_becas')->insert([
            'nombreLineaBecas' => "Desarrollo de aplicaciones para dispositivos móviles",
        ]);
        DB::table('lineas_becas')->insert([
            'nombreLineaBecas' => "Educación a Distancia",
        ]);
        DB::table('lineas_becas')->insert([
            'nombreLineaBecas' => "Servicios de TI",
        ]);
        DB::table('lineas_becas')->insert([
            'nombreLineaBecas' => "Recursos Educativos Digitales",
        ]);
        DB::table('lineas_becas')->insert([
            'nombreLineaBecas' => "Supercómputo",
        ]);
        DB::table('lineas_becas')->insert([
            'nombreLineaBecas' => "Desarrollo de medios audiovisuales y animación 3D",
        ]);
    }
}
