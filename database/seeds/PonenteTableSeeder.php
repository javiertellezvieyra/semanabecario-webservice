<?php

use Illuminate\Database\Seeder;

class PonenteTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('ponentes')->insert([
            'nombre' => "Armando",
            'apellidoPaterno' => "Líos",
            'apellidoMaterno' => "Prieto",
            'gradoAcademico' => "La calle",
            'semblanza' => Str::random(100),
            'nombreImagen' => "a",
        ]);
        DB::table('ponentes')->insert([
            'nombre' => "Dinorah",
            'apellidoPaterno' => "Casas",
            'apellidoMaterno' => "Rojo",
            'gradoAcademico' => "Maestría",
            'semblanza' => Str::random(100),
            'nombreImagen'=>"a",
        ]);
        DB::table('ponentes')->insert([
            'nombre' => "Nolasco",
            'apellidoPaterno' => "Barreto",
            'apellidoMaterno' => "Ceja",
            'gradoAcademico' => "Maestría",
            'semblanza' => Str::random(100),
            'nombreImagen' => "a",
        ]);
        DB::table('ponentes')->insert([
            'nombre' => "Adalia",
            'apellidoPaterno' => "Sauceda",
            'apellidoMaterno' => "Tovar",
            'gradoAcademico' => "Licenciatura",
            'semblanza' => Str::random(100),
            'nombreImagen' => "a",
        ]);
        DB::table('ponentes')->insert([
            'nombre' => "Luvencio",
            'apellidoPaterno' => "Alemán",
            'apellidoMaterno' => "Chávez",
            'gradoAcademico' => "Licenciatura",
            'semblanza' => Str::random(100),
            'nombreImagen' => "a",
        ]);
        DB::table('ponentes')->insert([
            'nombre' => "Loreto",
            'apellidoPaterno' => "Medrano",
            'apellidoMaterno' => "Toro Muuuuu",
            'gradoAcademico' => "La vida",
            'semblanza' => Str::random(100),
            'nombreImagen' => "a",
        ]);
    }
}
