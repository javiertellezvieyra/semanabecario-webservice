<?php

use Illuminate\Database\Seeder;

class ActividadesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::table('actividades')->insert([
            'nombreActividad' => "Cuando la realidad superó a la ilusión. Una experiencia como becario",
            'descripcion' => Str::random(100),
            'aula' => "Auditorio IIMAS",
            'cupoMaximo' => 100,
            'idSede' => 2,
            'idTipo' => 1,
            'linkCuestionario' => "https://docs.google.com/forms/d/e/1FAIpQLSfn1gWkL-XiogfxTFkk4lYp16K-BnVYVRcbt9wE8pAbUS-EEQ/viewform?usp=sf_link",
            'minutosTotales'=>240,
        ]);
        DB::table('actividades')->insert([
            'nombreActividad' => "¿Qué tanto conoces a tus usuarios?",
            'descripcion' => Str::random(100),
            'aula' => "Auditorio IIMAS",
            'cupoMaximo' => 100,
            'idSede' => 2,
            'idTipo' => 1,
            'linkCuestionario' => "https://docs.google.com/forms/d/e/1FAIpQLSfn1gWkL-XiogfxTFkk4lYp16K-BnVYVRcbt9wE8pAbUS-EEQ/viewform?usp=sf_link",
            'minutosTotales'=>270,
        ]);
        DB::table('actividades')->insert([
            'nombreActividad' => "Becarios como unidad de producción profesional.",
            'descripcion' => Str::random(100),
            'aula' => "Auditorio IIMAS",
            'cupoMaximo' => 100,
            'idSede' => 2,
            'idTipo' => 2,
            'linkCuestionario' => "https://docs.google.com/forms/d/e/1FAIpQLSfn1gWkL-XiogfxTFkk4lYp16K-BnVYVRcbt9wE8pAbUS-EEQ/viewform?usp=sf_link",
            'minutosTotales'=>90,

        ]);
        DB::table('actividades')->insert([
            'nombreActividad' => "Un entorno virtual para el trabajo",
            'descripcion' => Str::random(100),
            'aula' => "Auditorio IIMAS",
            'cupoMaximo' => 10,
            'idSede' => 2,
            'idTipo' => 3,
            'linkCuestionario' => "https://docs.google.com/forms/d/e/1FAIpQLSfn1gWkL-XiogfxTFkk4lYp16K-BnVYVRcbt9wE8pAbUS-EEQ/viewform?usp=sf_link",
            'minutosTotales'=>120,

        ]);
        DB::table('actividades')->insert([
            'nombreActividad' => "Elaboración de documentos digitales accesibles",
            'descripcion' => Str::random(100),
            'aula' => "Aula 11",
            'cupoMaximo' => 30,
            'idSede' => 3,
            'idTipo' => 3,
            'linkCuestionario' => "https://docs.google.com/forms/d/e/1FAIpQLSfn1gWkL-XiogfxTFkk4lYp16K-BnVYVRcbt9wE8pAbUS-EEQ/viewform?usp=sf_link",
            'minutosTotales'=>120,
        ]);
        DB::table('actividades')->insert([
            'nombreActividad' => "Firewall de aplicaciones Web con NGINX y ModSecurity",
            'descripcion' => Str::random(100),
            'aula' => "Sala de Videoconferencias",
            'cupoMaximo' => 20,
            'idSede' => 1,
            'idTipo' => 2,
            'linkCuestionario' => "https://docs.google.com/forms/d/e/1FAIpQLSfn1gWkL-XiogfxTFkk4lYp16K-BnVYVRcbt9wE8pAbUS-EEQ/viewform?usp=sf_link",
            'minutosTotales'=>120,
        ]);
    }
}
