<?php

use Illuminate\Database\Seeder;

class ActividadPonenteTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('actividad_ponente')->insert([
            'idPonente' => 1,
            'idActividad' => 1,
            'rol'=> "Conferencista",
        ]);
        DB::table('actividad_ponente')->insert([
            'idPonente' => 1,
            'idActividad' => 2,
            'rol' => "Conferencista"
        ]);
        DB::table('actividad_ponente')->insert([
            'idPonente' => 2,
            'idActividad' => 3,
            'rol' => "Tallerista"
        ]);
        DB::table('actividad_ponente')->insert([
            'idPonente' => 3,
            'idActividad' => 4,
            'rol' => "Moderador",
        ]);
        DB::table('actividad_ponente')->insert([
            'idPonente' => 4,
            'idActividad' => 4,
            'rol' => "Panelista"
        ]);
        DB::table('actividad_ponente')->insert([
            'idPonente' => 5,
            'idActividad' => 4,
            'rol' => "Panelista",
        ]);
        DB::table('actividad_ponente')->insert([
            'idPonente' => 6,
            'idActividad' => 4,
            'rol' => "Panelista",
        ]);
        DB::table('actividad_ponente')->insert([
            'idPonente' => 4,
            'idActividad' => 5,
            'rol' => "Moderador",
        ]);
        DB::table('actividad_ponente')->insert([
            'idPonente' => 3,
            'idActividad' => 5,
            'rol' => "Panelista"
        ]);
        DB::table('actividad_ponente')->insert([
            'idPonente' => 6,
            'idActividad' => 5,
            'rol' => "Panelista"
        ]);
        DB::table('actividad_ponente')->insert([
            'idPonente' => 2,
            'idActividad' => 6,
            'rol' => "Tallerista",
        ]);
    }
}
