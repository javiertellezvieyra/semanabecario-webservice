<?php

use Illuminate\Database\Seeder;

class SedesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('sedes')->insert([
            'siglas' => "DGTIC",
            'nombreSede' => "DIRECCIÓN GENERAL DE CÓMPUTO Y DE TECNOLOGÍAS DE INFORMACIÓN Y COMUNICACIÓN",
            'direccion' => "Universidad Nacional Autónoma de México, Ciudad Universitaria, Cto. Exterior s/n, C.U., 04510 Ciudad de México, CDMX",
            'ubicacion' => "DGTIC, Universidad Nacional Autónoma de México, Ciudad Universitaria, Circuito Exterior, University City, Mexico City, CDMX",
            'listaAulas' => "Aula 2, Aula 4, Aula 5",
            'rutaImg' => "img_1.jpg",
            'rutaCroquis' => "croquis_1.png",
        ]);
        DB::table('sedes')->insert([
            'siglas' => "IIMAS",
            'nombreSede' => "INSTITUTO DE INVESTIGACIONES EN MATEMÁTICAS APLICADAS Y EN SISTEMAS",
            'direccion' => "Circuto Escolar 3000, C.U., 04510 Ciudad de México, CDMX",
            'ubicacion' => "IIMAS (Instituto de Investigaciones en Matemáticas Aplicadas y en Sistemas), Circuto Escolar, Universidad Nacional Autónoma de México, Ciudad de México, CDMX",
            'listaAulas' => "Auditorio, Aula 14, Aula 15",
            'rutaImg' => "img_2.jpg",
            'rutaCroquis' => "croquis_2.png",
        ]);
        DB::table('sedes')->insert([
            'siglas' => "FCA",
            'nombreSede' => "FACULTAD DE CONTADURÍA Y ADMINISTRACIÓN",
            'direccion' => "Av. Universidad 3000, C.U., 04510 Ciudad de México, CDMX",
            'ubicacion' => "Facultad de Contaduría y Administración UNAM, Av. Universidad, Universidad Nacional Autónoma de México, Ciudad de México, CDMX",
            'listaAulas' => "Auditorio",
            'rutaImg' => "img_3.jpg",
            'rutaCroquis' => "croquis_3.png",
        ]);
    }
}
