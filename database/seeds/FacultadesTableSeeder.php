<?php

use Illuminate\Database\Seeder;

class FacultadesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('facultades')->insert([
            'nombreFacultad' => "Facultad de Arquitectura",
        ]);
        DB::table('facultades')->insert([
            'nombreFacultad' => "Facultad de Artes y Diseño",
        ]);
        DB::table('facultades')->insert([
            'nombreFacultad' => "Facultad de Ciencias",
        ]);
        DB::table('facultades')->insert([
            'nombreFacultad' => "Facultad de Ciencias Políticas y Sociales",
        ]);
        DB::table('facultades')->insert([
            'nombreFacultad' => "Facultad de Química",
        ]);
        DB::table('facultades')->insert([
            'nombreFacultad' => "Facultad de Contaduría y Administración",
        ]);
        DB::table('facultades')->insert([
            'nombreFacultad' => "Facultad de Derecho",
        ]);
        DB::table('facultades')->insert([
            'nombreFacultad' => "Facultad de Economía",
        ]);
        DB::table('facultades')->insert([
            'nombreFacultad' => "Escuela Nacional de Enfermería y Obstetricia",
        ]);
        DB::table('facultades')->insert([
            'nombreFacultad' => "ENES, Morelia, Mich.",
        ]);
        DB::table('facultades')->insert([
            'nombreFacultad' => "Facultad de Filosofía y Letras",
        ]);
        DB::table('facultades')->insert([
            'nombreFacultad' => "Facultad de Ingeniería",
        ]);
        DB::table('facultades')->insert([
            'nombreFacultad' => "Facultad de Medicina",
        ]);
        DB::table('facultades')->insert([
            'nombreFacultad' => "Facultad de Música",
        ]);
        DB::table('facultades')->insert([
            'nombreFacultad' => "Facultad de Odontología",
        ]);
        DB::table('facultades')->insert([
            'nombreFacultad' => "Escuela Nacional de Trabajo Social",
        ]);
        DB::table('facultades')->insert([
            'nombreFacultad' => "Facultad de Medicina Veterinaria y Zootecnia",
        ]);
        DB::table('facultades')->insert([
            'nombreFacultad' => "Facultad de Psicología",
        ]);
        DB::table('facultades')->insert([
            'nombreFacultad' => "Escuela Nacional del Lenguas, Lingüística y Traducción",
        ]);
        DB::table('facultades')->insert([
            'nombreFacultad' => "Facultad de Artes y Diseño, Taxco, Gro.",
        ]);
        DB::table('facultades')->insert([
            'nombreFacultad' => "Instituto de Biotecnología",
        ]);
        DB::table('facultades')->insert([
            'nombreFacultad' => "Instituto de Energías Renovables",
        ]);
        DB::table('facultades')->insert([
            'nombreFacultad' => "Centro de Nanociencias y Nanotecnología, Ensenada, B. C.",
        ]);
        DB::table('facultades')->insert([
            'nombreFacultad' => "FES Cuautitlán",
        ]);
        DB::table('facultades')->insert([
            'nombreFacultad' => "FES Acatlán",
        ]);
        DB::table('facultades')->insert([
            'nombreFacultad' => "FES Iztacala",
        ]);
        DB::table('facultades')->insert([
            'nombreFacultad' => "ENES Unidad León, Gto.",
        ]);
        DB::table('facultades')->insert([
            'nombreFacultad' => "FES Aragón",
        ]);
        DB::table('facultades')->insert([
            'nombreFacultad' => "FES Zaragoza",
        ]);
        DB::table('facultades')->insert([
            'nombreFacultad' => "FES Zaragoza Campus 3 Tlaxcala",
        ]);
        DB::table('facultades')->insert([
            'nombreFacultad' => "ENES, Mérida, Yuc.",
        ]);
        DB::table('facultades')->insert([
            'nombreFacultad' => "ENES, Juriquilla, Qro.",
        ]);
    }
}
