<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Asistencia extends Model
{
    protected $table = 'asistencias';


    /*Obtiene si un asistencia ya se registro*/
    public function getAsistencia($idAsistente, $idFecha){
    	$asistencia=Asistencia::where('idAsistente',$idAsistente)->where('idFecha',$idFecha)->first();

    	return $asistencia;

    }


}
