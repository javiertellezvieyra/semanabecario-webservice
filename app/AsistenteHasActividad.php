<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AsistenteHasActividad extends Model
{
     /**
     * La tabla asociada con el modelo.
     *
     * @var string
     */
    protected $table = 'asistente_has_actividad';
    /**
     * Indica si el modelo debe ser timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * Regresa todos los tokens de los usuarios inscritos a esa actividad
     */
    public function getTokens($id_actividad) {
        return AsistenteHasActividad::select('tokenAsistente','idAsistente')
            ->where('idActividad', $id_actividad)->get();
    }
}
