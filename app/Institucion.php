<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Institucion extends Model
{
	/**
     * La tabla asociada con el modelo.
     *
     * @var string
     */
    protected $table = 'instituciones';
    /**
     * Indica si el modelo debe ser timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
}
