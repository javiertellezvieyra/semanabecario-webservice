<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Asistente extends Model
{
    /**
     * La tabla asociada con el modelo.
     *
     * @var string
     */
    protected $table = 'asistentes';
    /**
     * Indica si el modelo debe ser timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'remember_token',
    ];

    /**
     * Obtiene todos los asistentes.
     */
    public function getAsistentes() {
        $asistentes["becarios"] = Asistente::join("asistentes_becarios", "asistentes_becarios.id", "=", "asistentes.id")->get();
        $asistentes["externos"] = Asistente::join("asistentes_externos", "asistentes_externos.id", "=", "asistentes.id")->get();
        return $asistentes;
    }

     public function notificaciones()    {
        return $this->hasMany('App\Notificaciones', 'idAsistente', 'id');
    }

}
