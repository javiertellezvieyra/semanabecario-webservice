<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AsistenteExterno extends Model
{
    /**
     * La tabla asociada con el modelo.
     *
     * @var string
     */
    protected $table = 'asistentes_externos';
    /**
     * Indica si el modelo debe ser timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
}
