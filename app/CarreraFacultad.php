<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CarreraFacultad extends Model
{

	/**
     * La tabla asociada con el modelo.
     *
     * @var string
     */
	protected $table = 'carrera_facultad';
	/**
     * Indica si el modelo debe ser timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    public function facultad(){
        return $this->belongsTo('App\Facultad','idFacultad', 'id');
    }

}
