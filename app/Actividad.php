<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Actividad extends Model
{
	/**
     * La tabla asociada con el modelo.
     *
     * @var string
     */
    protected $table = 'actividades';
    /**
     * Indica si el modelo debe ser timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * Obtiene el valor de la sede asociada a la actividad.
     */
    public function sede() {
        return $this->hasOne('App\Sede', 'idSede');
    }

    /**
     * Obtiene el valor del tipo asociado a la actividad.
     */
    public function tipo() {
        return $this->hasOne('App\Tipo', 'idTipo');
    }

    /**
     * Obtiene todas las actividades.
     */
    public function getActividades() {
        return Actividad::select('actividades.id','actividades.nombreActividad', 'tipos.nombreTipo')
        ->join('tipos', 'actividades.idTipo', '=', 'tipos.idTipo')->get();
    }

    /**
     * Obtiene los datos de una actividad
     */
    public function getDetalleActividad($id_actividad) {
        return Actividad::select('actividades.id','actividades.idTipo', 'tipos.nombreTipo', 'actividades.nombreActividad', 'actividades.idSede', 'actividades.aula', 'sedes.siglas', 'actividades.cupoMaximo', 'actividades.cupoActual', 'actividades.descripcion','actividades.nombreImagen','actividades.linkCuestionario')
        ->where('actividades.id', $id_actividad)
        ->join('tipos', 'actividades.idTipo', '=', 'tipos.idTipo')
        ->join('sedes', 'actividades.idSede', '=', 'sedes.id')
        ->first();
    }

    //obtiene los ponentes de una actividad
    public function ponentes(){
        return $this->hasMany('App\ActividadPonente');
    }

}
