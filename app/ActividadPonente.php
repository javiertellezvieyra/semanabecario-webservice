<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ActividadPonente extends Model
{
    //
    protected $table = 'actividad_ponente';

    public $timestamps = false;

    protected $fillable=['idActividad','idPonente','rol'];


    //obtiene los datos de la actividad donde participa el ponente
    public function actividad(){

        return $this->belongsTo('App\Actividad','foreign_key','idActividad');

    }

    //obtiene los datos de la actividad donde participa el ponente
    public function ponente(){

        return $this->belongsTo('App\Ponente','idPonente');

    }

}
