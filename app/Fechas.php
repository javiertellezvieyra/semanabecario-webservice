<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Fechas extends Model
{
    /**
     * La tabla asociada con el modelo.
     *
     * @var string
     */
    protected $table = 'fechas';
    /**
     * Indica si el modelo debe ser timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * Funcion que devuelve todas las fechas de una actividad
     * @param $id_actividad
     * 
     */
    public function getFechas($id_actividad){
        return Fechas::select('fechas.fecha', 'fechas.duracion')
                     ->where('fechas.idActividad', $id_actividad)
                     ->get();
    }

    /**
     * Funcion que devuelve un objeto fecha con base en una actividad y una fecha
     * @param $id_actividad, $fecha
     * 
     */
    public function getFecha($fecha,$idActividad){
        $fecha=$fecha->format('Y-m-d');
        return Fechas::where('idActividad',$idActividad)->where('fecha','like',$fecha.'%')->first();
    }
}
