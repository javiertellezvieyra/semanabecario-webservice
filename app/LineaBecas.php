<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LineaBecas extends Model
{
	/**
     * La tabla asociada con el modelo.
     *
     * @var string
     */
    protected $table = 'lineas_becas';
    /**
     * Indica si el modelo debe ser timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

     public function asistenteBecario()    {
        return $this->hasMany('App\AsistenteBecario', 'idLineaBecas','id');
    }
}
