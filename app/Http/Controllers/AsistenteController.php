<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Asistente;
use App\AsistenteBecario;
use App\AsistenteExterno;
use App\CarreraFacultad;
use App\LineaBecas;
use App\Actividad;
use App\Fechas;
use App\AsistenteHasActividad;
use App\Asistencia;
use DateTime;
use Hashids;
use App\Http\Requests\RegistrarAsistenciasRequest;

class AsistenteController extends Controller
{
    /**
     * Registra a un asistente.
     * Tipo asistente:
     * 1 - becario
     * 2 - externo
     */
    public function registrarAsistente(Request $request) {
        $tipoAsistente = $request->input('tipoAsistente');
        $nombre = $request->input('nombre');
    	$apPaterno = $request->input('apellidoPaterno');
    	$apMaterno = $request->input('apellidoMaterno');
    	$correo = $request->input('correo');
        $telefonoParticular = $request->input('telefonoParticular');
        $telefonoCelular = $request->input('telefonoCelular');

        // Validar correo
        if (Asistente::where('correo', '=', $correo)->exists()) {
            return ['response' => "Ya se registró este correo."];
        }

        // Si el validador pasa, registramos el asistente.
        $asistente = new Asistente();
        $asistente->nombre = $nombre;
        $asistente->apellidoPaterno = $apPaterno;
        $asistente->apellidoMaterno = $apMaterno;
        $asistente->correo = $correo;
        $asistente->telefonoParticular = $telefonoParticular;
        $asistente->telefonoCelular = $telefonoCelular;
        $asistente->tipoAsistente = $tipoAsistente;
        $asistente->save();

        // Revisar el tipo de asistente
        switch ($tipoAsistente) {
            case '1':
                $semestre = $request->input('semestre');
                $idLineaBecas = $request->input('idLineaBecas');
                $idFacultad = $request->input('idFacultad');
                $idCarrera = $request->input('idCarrera');

                $becario = new AsistenteBecario();
                $becario->id = $asistente->id;
                $becario->semestre = $semestre;
                $becario->idLineaBecas = $idLineaBecas;
                $becario->idFacultad = $idFacultad;
                $becario->idCarrera = $idCarrera;
                $becario->save();
                break;
            case '2':
                $procedencia = $request->input('procedencia');

                $externo = new AsistenteExterno();
                $externo->id = $asistente->id;
                $externo->procedencia = $procedencia;
                $externo->save();
                break;
            default:
                return ['response' => "Error en el registro"];
                break;
        }

        return ['response' => "El registro se realizó con éxito.", 'idAsistente' => $asistente->id];
    }

    /**
     * Muestra todos los asistentes registrados.
     */
    public function obtenerAsistentes() {
        $asistente = new Asistente();
        return \Response::json($asistente->getAsistentes());
    }

    /**
     * Muestra la lista de actividades a las cuales está inscrito el
     * asistente.
     */
    public function misActividades(Request $request){

        $asistente=$request->id_asistente;
        $arreglo = Array();
        // Verificamos que el asistente esté registrado.
        if (empty($request->id_asistente)) {
            array_push($arreglo, ['code' => 400]);
            return \Response::json($arreglo);
        }
        $actividades = AsistenteHasActividad::join('actividades','actividades.id','=','asistente_has_actividad.idActividad')
            ->join('sedes', 'sedes.id', '=', 'actividades.idSede')
            ->where('asistente_has_actividad.idAsistente',$asistente)
            ->get();

        //revisa que el asistente este inscrito a actividades
        if (count($actividades)<1) {
            array_push($arreglo, ['code' => 400]);
            return \Response::json($arreglo);
        }

        $fecha = new Fechas();
        //Obtenemos todas las actividades
        $misActividades = Array();
        //Armamos arreglos asociativos para organizar la informacion de cada una de las actividades
        foreach($actividades as $actividad){
            $idActividad = $actividad->idActividad;
            $fechas = $fecha->getFechas($idActividad);
            $actFinal = Array();
            $actFinal['idActividad']     = $idActividad;
            $actFinal['nombreActividad'] = $actividad->nombreActividad;
            $actFinal['fecha']           = $fechas;
            $actFinal['siglas']          = $actividad->siglas;
            $actFinal['aula']            = $actividad->aula;
            array_push($misActividades, $actFinal);
        }
        array_push($arreglo, ['code' => 200]);
        array_push($arreglo, ['actividades' => $misActividades]);
        return \Response::json($arreglo);

    }
      /**
     * Muestra la lista de notificaciones de un
     * asistente.
     */
    public function misNotificaciones(Request $request){
        $asistente=$request->id_asistente;
        $response=Array();
        // Verificamos que el asistente esté registrado.
        if (empty($request->id_asistente)) {
            array_push($response, ['code' => 400]);
            return \Response::json($response);
        }
        
        $misNotificaciones = Array();
        $notifiaciones= Asistente::find($asistente)->notificaciones;

        foreach($notifiaciones as $notificacion){
            $notFinal = Array();
            $notFinal['titulo']= $notificacion->titulo;
            $notFinal['texto'] = $notificacion->texto;
            $notFinal['accion']= $notificacion->accion;
            array_push($misNotificaciones, $notFinal);
        }
        array_push($response, ['code' => 200]);
        array_push($response, ['notifiaciones' => $misNotificaciones]);
        return \Response::json($response);
    }


    /*Regresa la vista con el listado de asistentes*/
    public function verAsistentes(){
        $asistentes=new Asistente();
        $asistentes=$asistentes->getAsistentes();


        $datosAsistentes=array();
         $datosAsistentes['becarios']=array();
        if(!is_null($asistentes['becarios'])){
            foreach ($asistentes['becarios'] as $becario) {
                $lineaBeca=LineaBecas::find($becario->idLineaBecas);

                $datosAsistentes['becarios'][]=['id'=>$becario->id,
                                    'nombre'=>$becario->nombre.' '.$becario->apellidoPaterno,
                                    'correo'=>$becario->correo,
                                    'lineaBeca'=>$lineaBeca->nombreLineaBecas,
                                    ];
            }
        }

        $datosAsistentes['externos']=array();
        foreach ($asistentes['externos'] as $externo) {

            $datosAsistentes['externos'][]=['id'=>$externo->id,
                                'nombre'=>$externo->nombre.' '.$externo->apellidoPaterno,
                                'correo'=>$externo->correo,
                                ];
        }

        //$datosAsistentes['becarios']);
        return view('asistentes.verAsistentes',compact('datosAsistentes'));
    }

    /*Regresa la vista con la informacion de un asistente*/
    public function detalleAsistente($asistente){
        $tipoAsistente=Asistente::find($asistente);
        $horas=(int)($tipoAsistente->minutosAsistidos/60);
        $minutos=$tipoAsistente->minutosAsistidos%60;
        if($minutos<10){
            $minutos='0'.$minutos;
        }

        $datosAsistente=['Nombre'=>$tipoAsistente->nombre.' '.$tipoAsistente->apellidoPaterno.' '.$tipoAsistente->apellidoMaterno,
                        'Correo'=>$tipoAsistente->correo,
                        'Teléfono particular'=>$tipoAsistente->telefonoParticular,
                        'Teléfono celular'=>$tipoAsistente->telefonoCelular,
                        'Horas asistidas'=>$horas.':'.$minutos];

        if($tipoAsistente->tipoAsistente==1){
            $becario=AsistenteBecario::find($asistente);
            $datosAsistente['Semestre']=$becario->semestre;
            $datosAsistente['Línea de becas']=$becario->lineaBecas->nombreLineaBecas;
            $carreraFacultad=CarreraFacultad::where('idCarrera','=',$becario->idCarrera)->where('idFacultad','=',$becario->idFacultad)->first();
            $datosAsistente['Carrera']=$carreraFacultad->nombreCarrera;
            $datosAsistente['Facultad']=$carreraFacultad->facultad->nombreFacultad;
        }else{
            $datosAsistente['Procedencia']=AsistenteExterno::find($asistente)->procedencia;
        }

        return view('asistentes.detalleAsistente',compact('datosAsistente'));
    }


    /**
     * Metodo auxiliar para registrarAsistencia que suma los minutos de la actividad
     * a los minutos de asistencia totales del asistente 
     */
    public function sumaHorasTotales($id_asistente, $horas){
        $asistente = Asistente::find($id_asistente);
        $asistente->minutosAsistidos += $horas;
        $asistente->save();
    }

    /**
     * Metodo auxiliar para registrarAsistencia que suma los minutos de la actividad
     * al contador de minutos de la actividad 
     */
    public function sumaHorasActividad($id_asistente, $id_actividad, $horas){
        $asistente_has_actividad = AsistenteHasActividad::where('idActividad', $id_actividad)
                                                         ->where('idAsistente', $id_asistente)
                                                         ->first();
        $asistente_has_actividad->contadorMinutos += $horas;
        $asistente_has_actividad->save();  
    }

    /**
     * Registra una asistencia a un usuario en una actividad especifica
     *@param $request en donde se encontrara el codigo 
     */
    public function registrarAsistencia(Request $request){
        $mensaje=array();
        $ids=$request->cadena;
        $ids=explode("-", $ids);
        $id_actividad=Hashids::decode($ids[0])[0];
        $id_asistente=Hashids::decode($ids[1])[0];
        $fe = new Fechas();
        $timestamp = new \DateTime();
        $dia = $timestamp->format('Y-m-d');
        $fe=$fe->getFecha($timestamp,$id_actividad);
        if(!empty($fe)){
            $asistencia=new Asistencia();
            //dd($asistencia->getAsistencia($id_asistente,$fe->id));
            $asistencia=$asistencia->getAsistencia($id_asistente,$fe->id);
            if(!$asistencia->asistencia){
                $horasASumar=$fe->duracion;
                $this->sumaHorasActividad($id_asistente, $id_actividad, $horasASumar);
                $this->sumaHorasTotales($id_asistente, $horasASumar);
                $asistencia->asistencia=true;
                $asistencia->save();
                $mensaje["respuesta"]="exito";
            }else{
                $mensaje["respuesta"]="registrado";
            }
        }else{
            $mensaje["respuesta"]="fracaso";
        }

    
    return \Response::json($mensaje);
    }

  /**
     * Vista para tomar asistencia.
     */
    public function tomarAsistencia() {
        return view('asistencia.tomarAsistencia');
    }


  /**
  Metodo para tomar la asistencia de un arreglo de codigos
  **/  

    public function registrarAsistencias(RegistrarAsistenciasRequest $request){
        
        $codigos=$request->codigos;
        $registrados=array();
        $fracasos=array();
        $timestamp = new \DateTime();
        $dia = $timestamp->format('Y-m-d');
        
        foreach ($codigos as $codigo) {
            $ids=explode("-", $codigo);
            if(count($ids)<2){
                $fracasos[]=$codigo;
            }else{
                $id_actividad=Hashids::decode($ids[0]);
                $id_asistente=Hashids::decode($ids[1]);
                if(empty($id_actividad) || empty($id_asistente)){
                    $fracasos[]=$codigo;
                }else{
                    $id_actividad=$id_actividad[0];
                    $id_asistente=$id_asistente[0];
                    $fe = new Fechas();
                    $fe=$fe->getFecha($timestamp,$id_actividad);
                    if(!empty($fe)){ 
                        $asistencia=new Asistencia();
                        $asistencia=$asistencia->getAsistencia($id_asistente,$fe->id);
                        if(!empty($asistencia)){
                            if(!$asistencia->asistencia){
                                $horasASumar=$fe->duracion;
                                $this->sumaHorasActividad($id_asistente, $id_actividad, $horasASumar);
                                $this->sumaHorasTotales($id_asistente, $horasASumar);
                                $asistencia->asistencia=true;
                                $asistencia->save();
                            }else{
                                $registrados[]=$codigo;
                            }
                        }else{
                           $fracasos[]=$codigo; 
                        }
                    }else{
                        $fracasos[]=$codigo;
                    }
                }
            }
        }
        $exito="";
        if (empty($registrados) && empty($fracasos)) {
            $exito="exito";
        }
        return view('/asistencia/tomarAsistencia', compact('registrados', 'fracasos','exito'));
    }



}






