<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\SedesRegistrarRequest;
use App\Sede;

class SedesController extends Controller
{
	/**
	 * Muestra todas las sedes en formato JSON.
	 */
    public function obtenerSedes() {
    	$sedes = Sede::all();
    	return \Response::json($sedes);
    }

    /**
     * Muestra el detalle de una sede en formato JSON.
     */
    public function detalleSede(Request $request) {
        $sede = Sede::find($request->id_sede);
        return \Response::json($sede);
    }

    /**
     * Muestra todas las sedes.
     */
    public function verSedes() {
    	$sedes = Sede::all();
    	return view('sedes.sedes', compact('sedes'));
    }

    /**
     * Muestra el detalle de una sede.
     */
    public function verSede(Request $request) {
    	$id_sede = $request->id;
    	$sede = Sede::find($id_sede);
        return view('sedes.detalleSede', compact('sede'));
    }

    /**
     * Elimina una sede.
     */
    public function eliminarSede(Request $request) {
        $id_sede = $request->id;
        $sede = Sede::find($id_sede);
        $sede->delete();
        return redirect()->route('/sedes/verSedes')->with('success','Sede eliminada exitosamente');
    }

    /**
     * Vista para agregar una sede.
     */
    public function altaSede(Request $request) {
    	return view('sedes.altaSede');
    }

    /**
     * Inserta una sede.
     */
    public function registrarSede(SedesRegistrarRequest $request) {

        $idSede= Sede::all()->last()->id+1;
        // Se genera el nombre de la imagen de la sede según su id
        $rutaImg = 'img_'.$idSede.'.'.request()->rutaImg->getClientOriginalExtension();
        // Se genera el nombre del croquis de la sede según su id
        $rutaCroquis = 'croquis_'.$idSede.'.'.request()->rutaCroquis->getClientOriginalExtension();

    	//Se inserta sede
    	$sede=Sede::firstOrCreate(array(
    		'siglas' => $request->siglas,
    		'nombreSede'=>$request->nombreSede,
    		'direccion'=>$request->direccion,
    		'ubicacion'=>$request->ubicacion,
    		'listaAulas'=>$request->listaAulas,
            'rutaImg'=>$rutaImg,
            'rutaCroquis'=>$rutaCroquis,
    		));

        // Guarda la imagen nombrada por el id de la sede
        $request->rutaImg->move(storage_path('/app/public/sedes'),$rutaImg);
        // Guarda la imagen del croquis nombrada por el id de la sede
        $request->rutaCroquis->move(storage_path('/app/public/sedes'),$rutaCroquis);

    	return redirect()->route('/sedes/verSedes')->with('success','Sede '.$request->siglas.' registrada exitosamente');
    }

    /**
     * Actualiza una sede.
     */
    public function editarSede(Request $request) {
    	$request->validate([
            'siglas'=>'required',
            'nombreSede'=>'required',
            'direccion'=>'required',
            'ubicacion'=>'required',
            'listaAulas'=>'required',
        ]);

        $sede=Sede::find($request->id_sede);
        $sede->siglas = $request->siglas;
        $sede->nombreSede = $request->nombreSede;
        $sede->direccion = $request->direccion;
    	$sede->ubicacion = $request->ubicacion;
    	$sede->listaAulas = $request->listaAulas;

        // Imagen
        if (!is_null($request->rutaImg)) {
            $request->validate(['rutaImg'=>'image|mimes:jpeg,jpg,png']);
            // Se genera el nombre de la imagen de la sede según su id
            $rutaImg = 'img_'.$sede->id.'.'.request()->rutaImg->getClientOriginalExtension();
            // Guarda la imagen nombrada por el id de la sede
            $request->rutaImg->move(storage_path('/app/public/sedes'),$rutaImg);
            $sede->rutaImg = $rutaImg;
        }

        // Croquis
        if (!is_null($request->rutaCroquis)) {
            $request->validate(['rutaCroquis'=>'image|mimes:jpeg,jpg,png']);
            // Se genera el nombre del croquis de la sede según su id
            $rutaCroquis = 'croquis_'.$sede->id.'.'.request()->rutaCroquis->getClientOriginalExtension();
            // Guarda la imagen del croquis nombrada por el id de la sede
            $request->rutaCroquis->move(storage_path('/app/public/sedes'),$rutaCroquis);
            $sede->rutaCroquis = $rutaCroquis;
        }

        $sede->save();

    	return redirect()->route('/sedes/verSedes')->with('success','Sede editada exitosamente');
    }


}
