<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\{Actividad, Sede, Tipo, Ponente, Fechas, AsistenteHasActividad, ActividadPonente,Asistencia,Notificaciones};
use App\Http\Requests\ActividadesRegistrarRequest;
use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use FCM;

class ActividadController extends Controller
{
    /**
     * Regresa todas las actividades.
     */
    public function obtenerActividades() {
        $actividad = new Actividad();
        $fecha = new Fechas();
        $ponente = new Ponente();
        //Obtenemos todas las actividades
        $datosActividad = $actividad->getActividades();
        $arreglo = Array();
        //Armamos arreglos asociativos para organizar la informacion de cada una de las actividades
        foreach($datosActividad as $actividad){
            $idActividad = $actividad->id;
            $ponentes = $ponente->getPrimerPonenteActividad($idActividad);
            $nombrePonente = $ponentes->nombre;
            $nombrePonente .= " ". $ponentes->apellidoPaterno;
            $nombrePonente .= " ". $ponentes->apellidoMaterno;
            $fechas = $fecha->getFechas($idActividad);
            $actFinal = Array();
            $actFinal['idActividad']     = $idActividad;
            $actFinal['nombreActividad'] = $actividad->nombreActividad;
            $actFinal['ponente']         = $nombrePonente;
            $actFinal['nombreTipo']      = $actividad->nombreTipo;
            $actFinal['fecha']           = $fechas;
            array_push($arreglo, $actFinal);
        }
    	return \Response::json($arreglo);
    }

    /**
    * Regresa el detalle de una actividad.
    */
    public function detalleActividad(Request $request) {
        $actividad = new Actividad();
        $fecha = new Fechas();
        $ponente = new Ponente();
        $id_actividad = $request->id_actividad;
        //Obtenemos los datos de una actividad
        $verActividad = $actividad->getDetalleActividad($id_actividad);
        $fechas = $fecha->getFechas($id_actividad);
        $ponentes = $ponente->getPonentesActividad($id_actividad);
        $arregloPonentes = Array();
        foreach($ponentes as $participante){
            $datosPonente = Array();
            $datosPonente['id']              = $participante->id;
            $datosPonente['rol']             = $participante->rol;
            $datosPonente['nombre']          = $participante->nombre;
            $datosPonente['apellidoPaterno'] = $participante->apellidoPaterno;
            $datosPonente['apellidoMaterno'] = $participante->apellidoMaterno;
            array_push($arregloPonentes, $datosPonente);
        }
        $arreglo = Array();
        //Armamos el arreglo asociativo en el cual se basará el Json a enviar
        $arreglo['idTipo']          = $verActividad->idTipo;
        $arreglo['nombreTipo']      = $verActividad->nombreTipo;
        $arreglo['nombreActividad'] = $verActividad->nombreActividad;
        $arreglo['fechas']          = $fechas;
        $arreglo['idSede']          = $verActividad->idSede;
        $arreglo['nombreSede']      = $verActividad->siglas;
        $arreglo['aula']            = $verActividad->aula;
        $arreglo['cupo']            = $verActividad->cupoMaximo;
        $arreglo['numeroInscritos'] = $verActividad->cupoActual;
        $arreglo['ponentes']        = $arregloPonentes;
        $arreglo['descripcion']     = $verActividad->descripcion;
        $arreglo['nombreImagen']    = $verActividad->nombreImagen;
        $arreglo['linkCuestionario']= $verActividad->linkCuestionario;
    	return \Response::json($arreglo);
    }

     /**
     * Metodo auxiliar para inscribir asistente y para verificarInscripcion que verifica si el asistente
     * ya esta inscrito en la actividad
     * @param $id_actividad, $id_asistente
     */
    public function estaInscrito($id_actividad, $id_asistente){
        $asistente_has_actividad = AsistenteHasActividad::where('idAsistente', $id_asistente)->get();
        foreach($asistente_has_actividad as $actividad){
            if($actividad->idActividad == $id_actividad)
                //ya está inscrito
                return true;
        }
        //no está inscrito
        return false;
    }

    //metodo para verficar si aun hay cupo en una actividad
    public function hayCupo($id_actividad){
        $actividad=Actividad::find($id_actividad);
        if($actividad->cupoActual+1<=$actividad->cupoMaximo){
            return true;
        }elseif ($actividad->cupoMaximo==-1) {
            return true;
        }else{
            return false;
        }
    }

    /**
     * Responde a la aplicacion si un asistente ya está inscrito en la actividad
     */
    public function verificarInscripcion(Request $request){
        $id_actividad = (int)$request->idActividad;
        $id_asistente = (int)$request->idAsistente;
        $respuesta['respuesta'] = $this->estaInscrito($id_actividad, $id_asistente);

        return \Response::json($respuesta);
    }

    /**
     * Inscribe un asistente a una actividad
     */
    public function inscribirAsistente(Request $request){
        $id_actividad = (int)$request->idActividad;
        $id_asistente = (int)$request->idAsistente;
        $tokenAsistente = $request->tokenAsistente;
        $asistente_has_actividad = new AsistenteHasActividad();
        $asistente_has_actividad->idAsistente = $id_asistente;
        $asistente_has_actividad->idActividad = $id_actividad;
        $asistente_has_actividad->tokenAsistente = $tokenAsistente;
        $inscrito = $this->estaInscrito($id_actividad, $id_asistente);
        if(!$this->estaInscrito($id_actividad, $id_asistente)&& $this->hayCupo($id_actividad)){
            $asistente_has_actividad->save();
            //éxito al inscribir
            $actividad=Actividad::find($id_actividad);
            $actividad->cupoActual+=1;
            $actividad->save();
            $fechas=Fechas::where('fechas.idActividad', $id_actividad)->get();
            foreach ($fechas as $fecha) {
                $asistencias=new Asistencia();
                $asistencias->idAsistente=$id_asistente;
                $asistencias->idActividad=$id_actividad;
                $asistencias->idFecha=$fecha->id;
                $asistencias->save();
            }
            $respuesta['respuesta'] = true ;
        }else{
            //ya estaba inscrito
            $respuesta['respuesta'] = $inscrito;
        }
        return \Response::json($respuesta);
    }

    /**
     * Desinscribe a un asistente de una actividad
     */
    public function desinscribirAsistente(Request $request){
        $id_actividad = (int)$request->idActividad;
        $id_asistente = (int)$request->idAsistente;
        if($this->estaInscrito($id_actividad, $id_asistente)){
            $asistente_has_actividad = AsistenteHasActividad::where('idAsistente', $id_asistente)
                                                        ->where('idActividad', $id_actividad)
                                                        ->first();
            $id_asistente_has_actividad= $asistente_has_actividad->id;
            $a_morir = AsistenteHasActividad::find($id_asistente_has_actividad);
            $a_morir->delete();

            $actividad=Actividad::find($id_actividad);
            $actividad->cupoActual-=1;
            $actividad->save();
            $fechas=Asistencia::where('idActividad', $id_actividad)->where('idAsistente',$id_asistente)->delete();
            //Se desinscribio con exito
            $respuesta['respuesta'] = true;
        }else{
            //El alumno no estaba inscrito
            $respuesta['respuesta'] = false;
        }
        return \Response::json($respuesta);
    }


    public function misActividades(Request $request){
        $asistente=$request->id_asistente;
        $actividades = AsistenteHasActividad::join('actividades','actividades.id','=','asistente_has_actividad.idActividad')->where('asistente_has_actividad.idAsistente',$asistente)->get();
        $fecha = new Fechas();
        //Obtenemos todas las actividades
        $arreglo = Array();
        //Armamos arreglos asociativos para organizar la informacion de cada una de las actividades
        foreach($actividades as $actividad){
            $idActividad = $actividad->id;
            $fechas = $fecha->getFechas($idActividad);
            $actFinal = Array();
            $actFinal['idActividad']     = $idActividad;
            $actFinal['nombreActividad'] = $actividad->nombreActividad;
            $actFinal['nombreTipo']      = $actividad->nombreTipo;
            $actFinal['fecha']           = $fechas;
            array_push($arreglo, $actFinal);
        }
        return \Response::json($arreglo);
    }

    /**
     * Vista para registrar una actividad.
     */
    public function altaActividad() {
    	$sedes = Sede::all();
    	$tipos = Tipo::all();
        $ponentes=Ponente::select('nombre','apellidoPaterno','apellidoMaterno','id')->get();
        return view('actividades.altaActividad', compact('sedes', 'tipos','ponentes'));
    }

    /**
     * Registra una actividad en la BD.
     */
    public function registrarActividad(ActividadesRegistrarRequest $request) {
        //validar cupo maximo si la actividad no es conferencia
        if($request->tipo>1){
            $this->validate($request, ['cupoMaximo' => 'integer|min:1']);
        }

        //return \Response::json($request->datepicker[0]);
//dd(!empty($request->fotografia));
    	$nombreActividad = $request->nombreActividad;
    	$descripcion = $request->descripcion;
    	$aula = $request->aula;
    	$cupoMaximo = $request->cupoMaximo;
    	$idTipo = $request->tipo;
    	$idSede = $request->sede;
        $duracion=$request->duracion;
        $linkCuestionario=$request->linkCuestionario;
        $minutosTotal=0;
        if(!$this->rolesValidos($idTipo, $request->roles)){
            return back()->withErrors("Los roles de los ponentes no corresponden a este tipo de actividad");
        }

        // crear nueva actividad
    	$actividad = new Actividad();

        if(!empty($request->fotografia)){
            //generar nombre de la imagen del ponente
            $idActividad= Actividad::all()->last()->id+1;
            $imgNombre=$idActividad.'.'.request()->fotografia->getClientOriginalExtension();
            $actividad->nombreImagen=$imgNombre;
            //guarda la imagen nombrada por el id del ponente
            $request->fotografia->move(storage_path('/app/public/actividades'),$imgNombre);
        }
    	// Inserción

        $actividad->nombreActividad = $nombreActividad;
        $actividad->descripcion = $descripcion;
        $actividad->aula = $aula;
        if($cupoMaximo!=null){
            $actividad->cupoMaximo = $cupoMaximo;}
        $actividad->idTipo = $idTipo;
        $actividad->idSede = $idSede;
        $actividad->minutosTotales=$minutosTotal;
        $actividad->linkCuestionario=$linkCuestionario;
        $actividad->save();


        for ($i=0; $i < count($request->datepicker); $i++) {
            $fechas = new Fechas();
            $fechas->idActividad = $actividad->id;
            $fechas->fecha = date($request->datepicker[$i]);
            $horaDuracion=explode(":",$duracion[$i]);
            $fechas->duracion=($horaDuracion[0]*60)+$horaDuracion[1];
            $minutosTotal+=$fechas->duracion;
            $fechas->save();
        }

        $actividad->minutosTotales=$minutosTotal;
        $actividad->save();

        //Registrar ponentes de la actividad
        for ($i=0; $i < count($request->ponentes); $i++) {
             ActividadPonente::firstOrCreate(['idActividad' =>$actividad->id,'idPonente'=>$request->ponentes[$i],'rol'=>$request->roles[$i]]);
        }

        return redirect('/actividades/verActividades');
    }

    /**
     * Muestra todas las actividades
     */
    public function verActividades() {
        $act = new Actividad();
        $actividades = $act->getActividades();
        $fechas = new Fechas();
        foreach ($actividades as $actividad) {
            $actividad["fechas"] = $fechas->getFechas($actividad->id);
        }

        return view('actividades.actividades', compact('actividades'));
    }

    /**
     * Regresa el detalle de una actividad.
     */
    public function verActividad(Request $request) {
        $act = new Actividad();
        $id_actividad = $request->id;
        $actividad = $act->getDetalleActividad($id_actividad);
        $tipos = Tipo::all();
        $sedes = Sede::all();
        $fechas = Fechas::where('idActividad', $id_actividad)->get();
        foreach ($fechas as $fecha) {
            $hora=(int)($fecha->duracion/60);
            $minutos=$fecha->duracion%60 ;
            if($hora<10){
                $fecha->duracion='0'.$hora.':';
            }else{
                $fecha->duracion=$hora.':';
            }
            if($minutos<10){
                $fecha->duracion.='0';
            }
            $fecha->duracion.=$minutos;


        }
        $roles=ActividadPonente::select('idPonente','rol','id')->where('idActividad',$id_actividad)->get();
        $ponentes=Ponente::select('nombre','apellidoPaterno','apellidoMaterno','id')->get();
        return view('actividades.detalleActividad', compact('actividad', 'tipos', 'sedes', 'fechas','roles','ponentes'));
    }

    /**
     * Elimina una actividad.
     */
    public function eliminarActividad(Request $request) {
        $id_actividad = $request->id;

        // Eliminar fechas
        $fechas = Fechas::where('idActividad', $id_actividad)->get();
        foreach ($fechas as $fecha) {
            $fecha->delete();
        }
        //Eliminar ponentes
        $ponentes = ActividadPonente::where('idActividad', $id_actividad)->get();
        foreach ($ponentes as $ponente) {
            $ponente->delete();
        }
        //Eliminar actividad
        $actividad = Actividad::find($id_actividad);
        $nombreActividad = $actividad->nombreActividad;
        $actividad->delete();

        // Notificar a los asistentes de la actividad
        $asistente_has_actividad = new AsistenteHasActividad();
        $allAsistentes = $asistente_has_actividad->getTokens($id_actividad);
        $tokens = [];
        foreach ($allAsistentes as $indAsistente) {
            array_push($tokens, $indAsistente->tokenAsistente);
        }

        if( sizeof($tokens) > 0) {
            $optionBuilder = new OptionsBuilder();
            $optionBuilder->setTimeToLive(60*20);
            $notificationBuilder = new PayloadNotificationBuilder('Aviso Importante');
            $notificationBuilder->setBody('La actividad ' . $nombreActividad . ' se ha cancelado.')->setSound('default');
            $dataBuilder = new PayloadDataBuilder();
            $dataBuilder->addData(['a_data' => 'my_data']);
            $option = $optionBuilder->build();
            $notification = $notificationBuilder->build();
            $data = $dataBuilder->build();
            $downstreamResponse = FCM::sendTo($tokens, $option, $notification, $data);
            $this->registrarNotificacion($allAsistentes,$notificationBuilder,0);
        }

        return redirect()->route('/actividades/verActividades')->with('success','Actividad eliminada exitosamente');
    }

    /**
     * Edita una actividad.
     */
    public function editarActividad(ActividadesRegistrarRequest $request) {
        $id_actividad = $request->id_actividad;
        $id_ponente_actividad= $request->id_actividad_ponente;
        $nombreActividad = $request->nombreActividad;
        $descripcion = $request->descripcion;
        $aula = $request->aula;
        $cupoMaximo = $request->cupoMaximo;
        $idTipo = $request->tipo;
        $idSede = $request->sede;
        $fechas=$request->datepicker;
        $idFechas=$request->idFecha;
        $ponentes=$request->ponentes;
        $roles=$request->roles;
        $linkCuestionario=$request->linkCuestionario;
        $duracion=$request->duracion;
        // Validar Datos
        if(!$this->rolesValidos($idTipo, $roles)){
            return back()->withErrors("Los roles de los ponentes no corresponden a este tipo de actividad");
        }
        // Edición de datos
        $actividad = Actividad::find($id_actividad);

        if(!empty($request->fotografia)){
            //generar nombre de la imagen del ponente
            $imgNombre=$id_actividad.'.'.request()->fotografia->getClientOriginalExtension();
            $actividad->nombreImagen=$imgNombre;
            //guarda la imagen nombrada por el id del ponente
            $request->fotografia->move(storage_path('/app/public/actividades'),$imgNombre);
        }

        $actividad->nombreActividad = $nombreActividad;
        $actividad->descripcion = $descripcion;
        $actividad->aula = $aula;
         if($cupoMaximo!=null){
            $actividad->cupoMaximo = $cupoMaximo;}
        $actividad->idTipo = $idTipo;
        $actividad->idSede = $idSede;
        $actividad->linkCuestionario = $linkCuestionario;

        /* Actualizar fechas por actividad. */
        $fechasActuales= Fechas::where('idActividad',$id_actividad)->count();

        $minutosTotal=0;
        $i=0;
        for (; $i < $fechasActuales; $i++) {
            $fecha = Fechas::find($idFechas[$i]);
            $fecha->fecha = date($request->datepicker[$i]);
            $horaDuracion=explode(":",$duracion[$i]);
            $fecha->duracion=($horaDuracion[0]*60)+$horaDuracion[1];
            $minutosTotal+=$fecha->duracion;
            $fecha->save();
        }

        for (; $i <count($fechas) ; $i++) {
            $fecha = new Fechas();
            $fecha->idActividad = $actividad->id;
            $fecha->fecha = date($request->datepicker[$i]);
            $horaDuracion=explode(":",$duracion[$i]);
            $fecha->duracion=($horaDuracion[0]*60)+$horaDuracion[1];
            $minutosTotal+=$fecha->duracion;
            $fecha->save();
        }

        $actividad->minutosTotales=$minutosTotal;
        $actividad->save();

        /*Actualizar ponentes de la actividad*/
        $ponentesActuales=ActividadPonente::where('idActividad',$id_actividad)->count();

        $j=0;
         for (; $j <$ponentesActuales; $j++) {
            $ponente =ActividadPonente::find($id_ponente_actividad);
            $ponente->idPonente = $ponentes[$j];
            $ponente->rol=$roles[$j];
            $ponente->save();
        }

        for (; $j <count($ponentes) ; $j++) {
            $ponente = new ActividadPonente();
            $ponente->idActividad=$id_actividad;
            $ponente->idPonente = $ponentes[$j];
            $ponente->rol = $roles[$j];
            $ponente->save();
        }

        // Notificar a los asistentes de la actividad
        $asistente_has_actividad = new AsistenteHasActividad();
        $allAsistentes = $asistente_has_actividad->getTokens($id_actividad);
        $tokens = [];
        foreach ($allAsistentes as $indAsistente) {
            array_push($tokens, $indAsistente->tokenAsistente);
        }

        if( sizeof($tokens) > 0) {
            
            $optionBuilder = new OptionsBuilder();
            $optionBuilder->setTimeToLive(60*20);
            $notificationBuilder = new PayloadNotificationBuilder('Aviso Importante');
            $notificationBuilder->setBody('Hay cambios en la actividad ' . $nombreActividad)->setSound('default');
            $dataBuilder = new PayloadDataBuilder();
            $dataBuilder->addData(['a_data' => 'my_data']);
            $option = $optionBuilder->build();
            $notification = $notificationBuilder->build();
            $data = $dataBuilder->build();
            $downstreamResponse = FCM::sendTo($tokens, $option, $notification, $data);
            $this->registrarNotificacion($allAsistentes,$notificationBuilder,$id_actividad);
        }

        return redirect()->route('/actividades/verActividades')->with('success','Actividad editada exitosamente');
    }

    //eliminar una fecha de una actividad
    public function eliminarFecha($idFecha,$idActividad){
        $fecha=Fechas::find($idFecha);
        $fecha->delete();
        return redirect('/actividades/verActividades/'.$idActividad);
    }

    //elimina el ponente de actividad en la tabla actividad_ponente
    public function eliminarPonente($id_actividad_ponente,$id_actividad){
        $ponente=ActividadPonente::find($id_actividad_ponente);
        $ponente->delete();
        return redirect('/actividades/verActividades/'.$id_actividad);
    }

    /**metodo para verificar que los roles correspondan al tipo de actividad
    */
    public function rolesValidos($tipoActividad, $roles){
        switch ($tipoActividad) {
            case 1://conferencia
                if(in_array("Tallerista", $roles) || in_array("Moderador", $roles) ||in_array("Panelista", $roles)){
                    return false;
                }
                break;
            case 2://taller
                if(in_array("Conferencista", $roles) || in_array("Moderador", $roles) ||in_array("Panelista", $roles)){
                    return false;
                }
                break;
            case 3://mesa redonda
                if(in_array("Conferencista", $roles) || in_array("Tallerista", $roles)){
                    return false;
                }
                break;
        }
        return true;
    }

    /**metodo para registrar las notificaciones que tiene un asistente
    */
    public function registrarNotificacion($asistentes,$cuerpoNotificacion,$id_actividad){
        foreach ($asistentes as $asistente) {
            $notificacion=new Notificaciones();
            $notificacion->titulo=$cuerpoNotificacion->getTitle();
            $notificacion->texto=$cuerpoNotificacion->getBody();
            $notificacion->accion = (string)$id_actividad;
            $notificacion->idAsistente =$asistente->idAsistente ;
            $notificacion->save();
        }
    }
}