<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\RegistrarPonenteRequest;
use App\Ponente;

class PonenteController extends Controller
{

    //Regresa la vista para ver todos los ponentes 
    public function verPonentes(){
        $ponentes=Ponente::select('nombre','apellidoPaterno','apellidoMaterno','id','nombreImagen')->get();
        return  view('ponente.verPonentes')->with('ponentes',$ponentes);
    }

    //muetra formulario para dar de alta un ponente
	public function altaPonente(){
    	return view('ponente.altaPonente');
    }

    //Registrar un ponente en la base de datos
    public function registrarPonente(RegistrarPonenteRequest $request){
        //genera el nombre de la imagen del ponente segun su id 
        $idPonente= Ponente::all()->last()->id+1;

        $imgNombre=$idPonente.'.'.request()->fotografia->getClientOriginalExtension();

    	//Se inserta ponente
    	$ponente=Ponente::firstOrCreate(array(
    		'nombre' => $request->nombre,
    		'apellidoPaterno'=>$request->apellidoPaterno,
    		'apellidoMaterno'=>$request->apellidoMaterno,
    		'gradoAcademico'=>$request->gradoAcademico,
    		'semblanza'=>$request->semblanza,
            'nombreImagen'=>$imgNombre,
    		));

    	//guarda la imagen nombrada por el id del ponente
    	$request->fotografia->move(storage_path('/app/public/ponentes'),$imgNombre);
        

    	$nombrePonente= $request->nombre.' '.$request->apellidoPaterno;
    	return redirect()->route('/ponente/verPonentes')->with('success','Ponente '.$nombrePonente.' registrdo exitosamente');

    }

    public function modificarPonente($ponente){
        // muestra la vista con los datos actuales del ponente para ser modificados
            $datosPonente=Ponente::where('ponentes.id',$ponente)->get();
            return view('ponente.modificarPonente',['ponente'=>$datosPonente]);
    }

    public function actualizarPonente(Request $request){
        $request->validate([
            'nombre'=>'string',
            'apellidoPaterno'=>'string',
            'apellidoMaterno'=>'string',
            'gradoAcademico'=>'required',
            'semblanza'=>'required'
        ]);

        $ponente=Ponente::find($request->id);
        $ponente->nombre= $request->nombre;
        $ponente->apellidoPaterno= $request->apellidoPaterno;
        $ponente->apellidoMaterno= $request->apellidoMaterno;
        $ponente->gradoAcademico= $request->gradoAcademico;
        $ponente->semblanza= $request->semblanza;

        if(!is_null($request->fotografia)){
            $request->validate(['fotografia'=>'image|mimes:jpeg,jpg,png']);
            //guarda la imagen nombrada por el id del ponente
            $imgNombre=$ponente->id.'.'.request()->fotografia->getClientOriginalExtension();
            //dd($imgNombre);
            $request->fotografia->move(storage_path('/app/public/ponentes'),$imgNombre);
            $ponente->nombreImagen=$imgNombre;
        }

        $ponente->save();

        $nombrePonente= $ponente->nombre.' '.$ponente->apellidoPaterno;
        return redirect()->route('/ponente/verPonentes')->with('success','Ponente '.$nombrePonente.' modificado exitosamente');
    }

    public function eliminarPonente($id){
        $ponente = Ponente::find($id);
        //dd($ponente);
        $nombrePonente= $ponente->nombre.' '.$ponente->apellidoPaterno;
        $ponente->delete();
        return redirect()->route('/ponente/verPonentes')->with('success','Ponente '.$nombrePonente.' eliminado exitosamente');
    }

    public function getPonente($ponente){
        $datosPonente=Ponente::find($ponente);
        return \Response::json($datosPonente);
    }
}
