<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SedesRegistrarRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'siglas' => 'required',
            'nombreSede'=>'required',
            'direccion'=>'required' ,
            'ubicacion' => 'required',
            'listaAulas'=>'required',
            'rutaImg'=>'image|mimes:jpeg,jpg,png',
            'rutaCroquis'=>'image|mimes:jpeg,jpg,png'
        ];
    }
}
