<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ActividadesRegistrarRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombreActividad' => 'required',
            'descripcion'=>'required',
            'aula'=>'required' ,
            'cupoMaximo' => 'required_unless:tipo,1',
            'sede'=>'required|integer|min:1',
            'tipo'=>'required|integer|min:1',
            'datepicker.*'=>'required|min:1 ',
            'roles.*'=>'required|min:1 ',
            'ponentes.*'=>'required|min:1 ',
            'duracion.*'=>'required',
            'fotografia'=>'image|mimes:jpeg,jpg,png',
            'linkCuestionario'=>'required'
            
        ];
    }

    //Regresa mensaje de error cundo no hay un cupo maximo y es de tipo taller o mesa redonda
    public function messages(){
        return [
            'cupoMaximo.required_unless' => 'El campo cupo maximo es obligatorio a menos que la actividad sea de tipo Conferencia',
            'datepicker.*.required'=>'El campo fecha es obligatorio.',
            'roles.*.required'=>'El campo rol es obligatorio.',  
            'ponentes.*.required'=>'El campo ponente es obligatorio.'
            
        ];
    }
}
