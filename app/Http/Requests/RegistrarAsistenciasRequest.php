<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegistrarAsistenciasRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'codigos.*' => 'required'
        ];
    }

      //Regresa mensaje de error cundo no hay un cupo maximo y es de tipo taller o mesa redonda
    public function messages(){
        return [
            'codigos.*.required'=>'El campo codigo es obligatorio.'            
        ];
    }
}
