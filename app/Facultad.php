<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Facultad extends Model
{
	/**
     * La tabla asociada con el modelo.
     *
     * @var string
     */
    protected $table = 'facultades';
    /**
     * Indica si el modelo debe ser timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    public function carreraFacultad()    {
        return $this->hasMany('App\CarreraFacultad', 'idFacultad','id');
    }
}
