<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AsistenteBecario extends Model
{
    /**
     * La tabla asociada con el modelo.
     *
     * @var string
     */
    protected $table = 'asistentes_becarios';
    /**
     * Indica si el modelo debe ser timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    public function lineaBecas(){
        return $this->belongsTo('App\LineaBecas','idLineaBecas', 'id');
    }
    
}
