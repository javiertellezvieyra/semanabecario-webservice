<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ponente extends Model
{
    /**
     * La tabla asociada con el modelo.
     *
     * @var string
     */
    protected $table = 'ponentes';
    /**
     * Indica si el modelo debe ser timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
    /**
     * Obtiene todos los ponentes de una actividad
     * @param $id_actividad, el identificador de la actividad
     */
    public function getPonentesActividad($id_actividad){
        return Ponente::select('ponentes.id','ponentes.nombre','ponentes.apellidoPaterno','ponentes.apellidoMaterno', 'actividad_ponente.rol')
                       ->where('actividad_ponente.idActividad', $id_actividad) 
                       ->join('actividad_ponente','ponentes.id','=','actividad_ponente.idPonente')
                       ->get();
    }
    /**
     * Obtiene un ponente para mostrarlo en el programa (no moredadores)
     * @param $id_actividad, el identificador de la actividad
     */
    public function getPrimerPonenteActividad($id_actividad){
        return Ponente::select('ponentes.nombre', 'ponentes.apellidoPaterno', 'ponentes.apellidoMaterno')
                        ->where('actividad_ponente.idActividad', $id_actividad)
                        ->where('actividad_ponente.rol','!=','Moderador')
                        ->join('actividad_ponente','ponentes.id', '=', 'actividad_ponente.idPonente')
                        ->first();
    }

     /*
    Campos para registrar un ponente
    */
    protected $fillable=['nombre','apellidoPaterno','apellidoMaterno','rol','gradoAcademico','semblanza','nombreImagen'];

    //Obtiene las actividades que tiene el ponente
    public function actividad(){
        return $this->hasMany('App\ActividadPonente');
    }


}
