<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sede extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'sedes';
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     *Campos para registrar una sede
     */
    protected $fillable=['siglas','nombreSede','direccion','ubicacion','listaAulas','rutaImg','rutaCroquis'];

    /**
     * Obtiene la actividad a la cual pertenece la sede.
     */
    public function actividad() {
        return $this->belongsTo('App\Actividad', 'idSede');
    }



}
