<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tipo extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'tipos';
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * Obtiene la actividad a la cual pertenece el tipo.
     */
    public function actividad() {
        return $this->belongsTo('App\Actividad', 'idTipo');
    }
}
