<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notificaciones extends Model
{
    
     /**
     * La tabla asociada con el modelo.
     *
     * @var string
     */
    protected $table = 'notificaciones';
    /**
     * Indica si el modelo debe ser timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

      public function asistente(){
        return $this->belongsTo('App\asistente');
    }
}
