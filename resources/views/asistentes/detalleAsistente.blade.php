@extends('layouts.app')

@section('content')
<h1>Asistentes</h1>
@foreach($datosAsistente as $dato)
  <div class="form-group">
    <p>
      <b>{{array_keys($datosAsistente, $dato)[0]}}: </b>
      {{$dato}}
    </p>
  </div>
@endforeach
@endsection