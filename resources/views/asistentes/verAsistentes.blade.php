@extends('layouts.admin')

@section('content')
<h1>Asistentes</h1>
@if(empty($datosAsistentes))
  <p>No hay asistentes.</p>
@else
  <h3>Becarios</h3>
  @forelse($datosAsistentes['becarios'] as $becario)
    <div class="form-group">
      <p><b>Nombre: </b>{{$becario['nombre']}}</p>
      <p><b>Correo: </b>{{$becario['correo']}}</p>
      <p><b>Línea de becas: </b>{{$becario['lineaBeca']}}</p>
    </div>
    <div class="input-group-append">
      <a class="btn btn-secondary" href="{{route('/asistentes/detalle', $becario['id'])}}">Ver Detalle</a>
    </div>
    <hr>
  @empty
  <p>No hay asistentes becarios.</p>
  @endforelse
  <h3>Externos</h3>
  @forelse($datosAsistentes['externos'] as $externo)
    <div class="form-group">
      <p><b>Nombre: </b>{{$externo['nombre']}}</p>
      <p><b>Correo: </b>{{$externo['correo']}}</p>
    </div>
    <div class="input-group-append">
      <a class="btn btn-secondary" href="{{route('/asistentes/detalle', $externo['id'])}}" >Ver Detalle</a>
    </div>
    <hr>
   @empty
  <p>No hay asistentes externos.</p>
  @endforelse
@endif

@endsection