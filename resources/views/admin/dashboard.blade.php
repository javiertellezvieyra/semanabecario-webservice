@extends('layouts.app')

@section('content')

<div class="row m-4">
	<a class="btn btn-primary btn-lg my-0 mx-auto" href="{{route('/actividades/verActividades')}}">Actividades</a>
</div>
<div class="row m-4">
	<a class="btn btn-primary btn-lg my-0 mx-auto" href="{{route('/ponente/verPonentes')}}" >Ponentes</a>
</div>
<div class="row m-4">
	<a class="btn btn-primary btn-lg my-0 mx-auto" href="{{route('/asistente/verAsistentes')}}">Asistentes</a>
</div>
<div class="row m-4">
	<a class="btn btn-primary btn-lg my-0 mx-auto" href="{{route('/sedes/verSedes')}}">Sedes</a>
</div>
<div class="row m-4">
	<a class="btn btn-primary btn-lg my-0 mx-auto" href="{{route('/asistencia/tomarAsistencia')}}">Tomar asistencia</a>
</div>
@endsection