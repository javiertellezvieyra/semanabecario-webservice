@extends('layouts.app')

@section('content')
<h1>Detalle de Sede</h1>
@if($errors->any())
      <div class="alert alert-danger">
        <ul>
          @foreach($errors->all() as $error)
            <li>{{$error}}</li>
          @endforeach
        </ul>
      </div>
@endif

<form method="post" action="{{route('/sedes/registrar')}}" enctype="multipart/form-data">
  @csrf
  <div class="form-group">
    <label for="siglas">Siglas</label>
    <input type="text" class="form-control" id="siglas" name="siglas">
  </div>
  <div class="form-group">
  	<label for="nombre">Nombre</label>
    <input type="text" class="form-control" id="nombreSede" name="nombreSede">
  </div>
  <div class="form-group">
  	<label for="direccion">Dirección</label>
    <input type="text" class="form-control" id="direccion" name="direccion">
  </div>
  <div class="form-group">
  	<label for="ubicacion">Ubicación</label>
    <input type="text" class="form-control" id="ubicacion" name="ubicacion">
  </div>
  <div class="form-group">
    <label for="listaAulas">Lista de Aulas (separadas por una coma)</label>
    <input type="text" class="form-control" id="listaAulas" name="listaAulas">
  </div>
  <div class="form-group">
    <label for="rutaImg">Imagen</label>
    <input type="file" class="form-control-file" id="rutaImg" name="rutaImg">
  </div>
  <div class="form-group">
    <label for="rutaCroquis">Croquis</label>
    <input type="file" class="form-control-file" id="rutaCroquis" name="rutaCroquis">
  </div>
  <button type="submit" class="btn btn-success">Registrar</button>
</form>

@endsection