@extends('layouts.admin')

@section('content')
<h1>Sedes</h1>
@if ($message = Session::get('success'))
<div class="alert alert-success alert-block">
  <button type="button" class="close" data-dismiss="alert">×</button>
  <strong>{{ $message }}</strong>
</div>
@endif

@if($errors->any())
<div class="alert alert-danger">
  <ul>
    @foreach($errors->all() as $error)
    <li>{{$error}}</li>
    @endforeach
  </ul>
</div>
@endif

<a class="btn btn-success" href="{{route('/sedes/altaSede')}}">Crear Sede</a>
<hr>
@forelse($sedes as $sede)
<div class="card mb-3">
  <div class="card-header">
    {{$sede->siglas}}
  </div>
  <div class="card-body">
    <h5 class="card-title">{{$sede->nombreSede}}</h5>
    <img src="{{asset('storage/sedes/'.$sede->rutaImg)}} " width="100px" height="100px" >
    <img src="{{asset('storage/sedes/'.$sede->rutaCroquis)}} " width="100px" height="100px" >
  </div>
  <div class="card-footer text-muted">
    <a href="{{route('/sedes/verSedes/detalle', $sede->id)}}" class="btn btn-primary">Ver Detalle</a>
  </div>
</div>
@empty
<p>No hay sedes.</p>
@endforelse
@endsection