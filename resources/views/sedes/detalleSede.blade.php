@extends('layouts.app')

@section('content')
<h1>Detalle de Sede</h1>
@if($errors->any())
      <div class="alert alert-danger">
        <ul>
          @foreach($errors->all() as $error)
            <li>{{$error}}</li>
          @endforeach
        </ul>
      </div>
@endif

<form method="post" action="{{route('/sedes/editarSede')}}" enctype="multipart/form-data">
  @csrf
  <div class="form-group">
  	<input type="hidden" id="id_sede" name="id_sede" value="{{$sede->id}}">
    <label for="siglas">Siglas</label>
    <input type="text" class="form-control" id="siglas" name="siglas" value="{{$sede->siglas}}">
  </div>
  <div class="form-group">
  	<label for="nombre">Nombre</label>
    <input type="text" class="form-control" id="nombreSede" name="nombreSede" value="{{$sede->nombreSede}}">
  </div>
  <div class="form-group">
  	<label for="direccion">Dirección</label>
    <input type="text" class="form-control" id="direccion" name="direccion" value="{{$sede->direccion}}">
  </div>
  <div class="form-group">
  	<label for="ubicacion">Ubicación</label>
    <input type="text" class="form-control" id="ubicacion" name="ubicacion" value="{{$sede->ubicacion}}">
  </div>
  <div class="form-group">
    <label for="listaAulas">Lista de Aulas</label>
    <input type="text" class="form-control" id="listaAulas" name="listaAulas" value="{{$sede->listaAulas}}">
  </div>
  <div class="form-group">
    <label for="rutaImg">Imagen</label>
    <img src="{{asset('storage/sedes/'.$sede->rutaImg)}} " width="100px" height="100px" >
    <input type="file" class="form-control-file" id="rutaImg" name="rutaImg">
  </div>
  <div class="form-group">
    <label for="rutaCroquis">Croquis</label>
    <img src="{{asset('storage/sedes/'.$sede->rutaCroquis)}}" width="100px" height="100px" >
    <input type="file" class="form-control-file" id="rutaCroquis" name="rutaCroquis">
  </div>

  <button type="submit" class="btn btn-success">Editar</button>
    <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#exampleModal">Eliminar</button>

</form>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Eliminar sede</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        ¿Desea eliminar la sede?
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
        <a class="btn btn-danger" href="{{route('/sedes/eliminar', ['id' => $sede->id])}}">Eliminar</a>
      </div>
    </div>
  </div>
</div>

@endsection