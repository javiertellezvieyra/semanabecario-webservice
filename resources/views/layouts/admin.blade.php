<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <title>Evento DGTIC</title>
    <script src="https://code.jquery.com/jquery-3.4.1.js" integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU=" crossorigin="anonymous"></script>
    <link rel="stylesheet" type="text/css" href="{{asset('css/app.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/styles.css')}}">
    <script type="text/javascript" src="{{asset('js/app.js')}}"></script>
    @yield('scripts')

</head>
<body>
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <a class="navbar-brand" href="{{route('/adminPanel')}}">Evento DGTIC</a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
            </ul>
            @if(Auth::check())
                <p class="text-secondary m-2">Admin</p>
                <form method="POST" action="{{route('logout')}}">
                    {{ csrf_field() }}
                    <button class="btn btn-danger">Cerrar sesión</button>
                </form>
            @else
                <div class="">
                    <a class="nav-link" href="{{url('/')}}">Home</a>
                </div>
            @endif
        </div>
    </nav>
<div class="m-5">
    <hr>
    <div class="row" style="height: 100%;">
        <!-- Menú de navegación -->
        <div class="col-3 lateral_menu">
            <div class="row m-4">
                <a class="btn btn-primary btn-block my-0" href="{{route('/actividades/verActividades')}}">Actividades</a>
            </div>
            <div class="row m-4">
                <a class="btn btn-primary btn-block my-0" href="{{route('/ponente/verPonentes')}}" >Ponentes</a>
            </div>
            <div class="row m-4">
                <a class="btn btn-primary btn-block my-0" href="{{route('/asistente/verAsistentes')}}">Asistentes</a>
            </div>
            <div class="row m-4">
                <a class="btn btn-primary btn-block my-0" href="{{route('/sedes/verSedes')}}">Sedes</a>
            </div>
            <div class="row m-4">
                <a class="btn btn-primary btn-block my-0" href="{{route('/asistencia/tomarAsistencia')}}">Tomar asistencia</a>
            </div>
        </div>
        <div class="col-9 sub_container">
            @if( session()->has('flash'))
            <div class="alert alert-info">
                {{ session('flash')}}
            </div>
            @endif
            @yield('content')
        </div>
    </div>
</div>
</body>
</html>