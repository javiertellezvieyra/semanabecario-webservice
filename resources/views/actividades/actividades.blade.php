@extends('layouts.admin')

@section('content')
<h1>Actividades</h1>
@if ($message = Session::get('success'))
  <div class="alert alert-success alert-block">
    <button type="button" class="close" data-dismiss="alert">×</button>
    <strong>{{ $message }}</strong>
  </div>
@endif

@if($errors->any())
      <div class="alert alert-danger">
        <ul>
          @foreach($errors->all() as $error)
            <li>{{$error}}</li>
          @endforeach
        </ul>
      </div>
@endif

	<a class="btn btn-success my-4" href="{{route('/actividades/altaActividad')}}">Crear Actividad</a>
	@forelse($actividades as $actividad)
    <div class="card mb-3">
      <div class="card-header">
        <b>Nombre:</b> {{$actividad->nombreActividad}}
      </div>
      <div class="card-body">
        <h5 class="card-title">{{$actividad->nombreTipo}}</h5>
        <h6 class="card-subtitle mb-2 text-muted">Fechas:</h6>
        @forelse($actividad->fechas as $fechas)
        <p class="card-text">{{$fechas->fecha}}</p>
        @empty
            <p>No hay fechas</p>
        @endforelse
        <a href="{{route('/actividades/verActividades/detalle', $actividad->id)}}" class="btn btn-primary">Ver Detalle</a>
      </div>
    </div>
	@empty
		<p>No hay actividades.</p>
	@endforelse
@endsection