@extends('layouts.app')

@section('scripts')
  <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script>
      $(function() {
          $('.datepicker').datepicker({
            dateFormat: 'yy-mm-dd',
            onSelect: function(datetext){
                var d = new Date(); // for now
                var h = d.getHours();
                h = (h < 10) ? ("0" + h) : h ;

                var m = d.getMinutes();
                m = (m < 10) ? ("0" + m) : m ;

                var s = d.getSeconds();
                s = (s < 10) ? ("0" + s) : s ;

                datetext = datetext + " " + h + ":" + m + ":" + s;
                $('.datepicker').val(datetext);
            },
        });
      });
  </script>

@endsection

@section('content')

<h1>Registro de una Actividad</h1>

@if($errors->any())
      <div class="alert alert-danger">
        <ul>
          @foreach($errors->all() as $error)
            <li>{{$error}}</li>
          @endforeach
        </ul>
      </div>
@endif

<form method="post" action="{{route('/actividades/registrar')}}" enctype="multipart/form-data">
  @csrf
  <div class="form-group">
    <label for="nombre">Nombre</label>
    <input type="text" class="form-control" id="nombreActividad" name="nombreActividad">
  </div>
  <div class="form-group">
    <label for="descripcion">Descripción</label>
    <textarea class="form-control" id="descripcion" name="descripcion"></textarea>
  </div>
  <div class="form-group">
    <label for="aula">Aula</label>
    <input type="text" class="form-control" id="aula" name="aula">
  </div>
  <div class="form-group">
    <label for="cupoMaximo">Cupo Máximo</label>
    <input type="number" class="form-control" id="cupoMaximo" name="cupoMaximo" min="0" >
  </div>
  <div class="form-group" id="date-form">
    <div class="row">
      <div class=" col-6">
        <label for="fecha">Fecha (aaaa-mm-dd hh:mm:ss)</label>
        <input type="text" class="form-control datepicker" name="datepicker[]">
      </div>
      <div class="col-6">
        <label for="duracion">Duración (hh:mm)</label>
        <input type="time" class="form-control" name="duracion[]" min="00:01" >
      </div>
    </div>
  </div>
  <button class="btn btn-secondary" id="addDate">Agregar fecha</button>
  <div class="form-group">
    <label for="tipo">Tipo</label>
    <select class="custom-select" name="tipo">
    	<option value="" selected="">--Seleccione--</option>
    	@foreach($tipos as $tipo)
            <option value="{{$tipo->idTipo}}">{{$tipo->nombreTipo}}</option>
        @endforeach
    </select>
  </div>

  <div class="form-group" id="ponentes">
    <label for="ponente" >Ponente</label>
    <div class="row">
      <div class=" col-md-6">
        <select class="custom-select" name="ponentes[]">
          <option value="" >--Seleccione ponente--</option>
          @foreach($ponentes->all() as $ponente)
            <option value='{{$ponente->id}}' >{{$ponente->nombre." ".$ponente->apellidoPaterno." ".$ponente->apellidoMaterno}}</option>
          @endforeach
        </select>
      </div>
      <div class="col-md-6">
        <select class="custom-select" name="roles[]">
          <option value="" >--Seleccione rol--</option>
          <option value="Tallerista" >Tallerista</option>
          <option value="Conferencista" >Conferencista</option>
          <option value="Moderador" >Moderador</option>
          <option value="Panelista" >Panelista</option>
        </select>
      </div>
    </div>
  </div>
  <button class="btn btn-secondary" id="agregarPonente">Agregar ponente</button>
  <div class="form-group">
    <label for="tipo">Sede</label>
    <select class="custom-select" name="sede">
    	<option value="" selected="">--Seleccione--</option>
    	@foreach($sedes as $sede)
            <option value="{{$sede->id}}">{{$sede->nombreSede}}</option>
        @endforeach
    </select>
  </div>
  <div class="form-group">
    <label for="nombre">Link de cuestionario</label>
    <input type="text" class="form-control" id="linkCuestionario" name="linkCuestionario">
  </div>
  <div class="form-group">
    <label for="tipo">Fotografía</label>
    <input type="file" class="form-control-file" name="fotografia" >
  </div>
  <button type="submit" class="btn btn-primary">Registrar</button>
</form>

<script type="text/javascript">

  $('#addDate').on('click', function(e){
    e.preventDefault();

    var fecha = '<div class="form-group"><div class="row"><div class=" col-md-6"><label for="fecha">Fecha</label><input type="text" class="form-control datepicker" name="datepicker[]"></div><div class="col-md-6"><label for="duracion">Duración</label><input type="time" class="form-control" name="duracion[]" min="00:01" ></div></div></div>';
    $('#date-form').append(fecha);

  });
  $('#agregarPonente').on('click',function(e){
    e.preventDefault();

    var ponente=' <div class="form-group"><label for="ponente" >Ponente</label><div class="row"><div class=" col-md-6"><select class="custom-select" name="ponentes[]"><option value="" >--Seleccione ponente--</option>@foreach($ponentes->all() as $ponente)<option value="{{$ponente->id}}" >{{$ponente->nombre." ".$ponente->apellidoPaterno." ".$ponente->apellidoMaterno}}</option>@endforeach</select></div><div class="col-md-6"><select class="custom-select" name="roles[]"><option value="" >--Seleccione rol--</option><option value="Tallerista" >Tallerista</option><option value="Conferencista" >Conferencista</option><option value="Moderador" >Moderador</option><option value="Panelista" >Panelista</option></select></div></div></div>';
    $('#ponentes').append(ponente);
  });

</script>
@endsection
