@extends('layouts.app')

@section('scripts')
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
  $(function() {
    $('.datepicker').datepicker({
      dateFormat: 'yy-mm-dd',
      onSelect: function(datetext){
                var d = new Date(); // for now
                var h = d.getHours();
                h = (h < 10) ? ("0" + h) : h ;

                var m = d.getMinutes();
                m = (m < 10) ? ("0" + m) : m ;

                var s = d.getSeconds();
                s = (s < 10) ? ("0" + s) : s ;

                datetext = datetext + " " + h + ":" + m + ":" + s;
                $('.datepicker').val(datetext);
              },
            });
  });
</script>
@endsection

@section('content')
<h1>Detalle de Actividad</h1>
@if($errors->any())
<div class="alert alert-danger">
  <ul>
    @foreach($errors->all() as $error)
    <li>{{$error}}</li>
    @endforeach
  </ul>
</div>
@endif

<form method="post" action="{{route('/actividades/editarActividad')}}" enctype="multipart/form-data">
  @csrf
  <div class="form-group">
  	<input type="hidden" id="id_actividad" name="id_actividad" value="{{$actividad->id}}">
    <label for="nombre">Nombre</label>
    <input type="text" class="form-control" id="nombreActividad" name="nombreActividad" value="{{$actividad->nombreActividad}}">
  </div>
  <div class="form-group">
    <label for="descripcion">Descripción</label>
    <textarea class="form-control" id="descripcion" name="descripcion">{{$actividad->descripcion}}</textarea>
  </div>
  <div class="form-group">
    <label for="aula">Aula</label>
    <input type="text" class="form-control" id="aula" name="aula" value="{{$actividad->aula}}">
  </div>
  <div class="form-group">
    <label for="cupoMaximo">Cupo Máximo</label>
    <input type="number" class="form-control" id="cupoMaximo" name="cupoMaximo" min="0" value="{{$actividad->cupoMaximo}}">
  </div>
  <div class="form-group" id="date-form">
    <label>Fechas</label>
    @forelse($fechas as $fecha)
    <div class="input-group">
      <input type="hidden" name="idFecha[]" value="{{$fecha->id}}">
        <div class="col">
          <label for="fecha">Fecha (aaaa-mm-dd hh:mm:ss)</label>
          <input type="text" class="form-control datepicker" name="datepicker[]" value="{{$fecha->fecha}}">
        </div>
        <div class="col">
          <label for="duracion">Duración (hh:mm)</label>
          <input type="time" class="form-control" name="duracion[]" min="00:01" value="{{$fecha->duracion}}">
        </div>
        <div class="col">
          <a class="btn btn-danger mt-4 mb-4 ml-2" href="{{route('/actividad/eliminarfecha',['idFecha'=>$fecha->id, 'idActividad'=>$actividad->id])}}">Eliminar</a>
        </div>
    </div>
    @empty
    <p>No hay fechas</p>
    @endforelse
  </div>
  <button class="btn btn-secondary" id="addDate">Agregar fecha</button>
  <div class="form-group">
    <label for="tipo">Tipo</label>
    <select class="custom-select" name="tipo">
    	@foreach($tipos as $tipo)
      @if ($actividad->idTipo == $tipo->idTipo)
      <option value="{{$actividad->idTipo}}" selected>{{$tipo->nombreTipo}}</option>
      @else
      <option value="{{$tipo->idTipo}}">{{$tipo->nombreTipo}}</option>
      @endif
      @endforeach
    </select>
  </div>
  <div class="form-group" id="ponentes">
    <!------->
    <label for="ponente" >Ponente</label>
    @foreach($roles as $rol)
    <input type="hidden" name="id_actividad_ponente" value="{{$rol->id}}">
    <div class="row">
      <div class=" col-md-6">
        <select class="custom-select" name="ponentes[]">
          @foreach($ponentes->all() as $ponente)
          <option value='{{$ponente->id}}' {{ $ponente->id == $rol->idPonente?"selected":""}}>
            {{$ponente->nombre." ".$ponente->apellidoPaterno." ".$ponente->apellidoMaterno}}
          </option>
          @endforeach
        </select>
      </div>
      <div class="col-md-6 input-group">
        <select class="custom-select" name="roles[]">
          @php $opcionesRol=["Tallerista","Conferencista","Moderador","Panelista"] @endphp
          @foreach($opcionesRol as $opcion)
          <option value="{{$opcion}}" {{strnatcasecmp($opcion,$rol->rol)==0?"selected":""}}>{{$opcion}}</option>
          @endforeach
        </select>
        <a class="btn btn-danger" href="{{route('/actividad/eliminarPonente',['id_actividad_ponente'=>$rol->id, 'id_actividad'=>$actividad->id])}}">Eliminar</a>
      </div>
    </div>
    @endforeach
  </div>
  <button class="btn btn-secondary" id="agregarPonente">Agregar ponente</button>
  <div class="form-group">
    <label for="tipo">Sede</label>
    <select class="custom-select" name="sede">
    	@foreach($sedes as $sede)
      @if ($actividad->idSede == $sede->id)
      <option value="{{$actividad->idSede}}" selected>{{$sede->nombreSede}}</option>
      @else
      <option value="{{$sede->id}}">{{$sede->nombreSede}}</option>
      @endif
      @endforeach
    </select>
  </div>
  <div class="form-group">
    <label for="descripcion">Link de cuestionario</label>
    <textarea class="form-control" id="linkCuestionario" name="linkCuestionario">{{$actividad->linkCuestionario}}</textarea>
  </div>
  <div class="form-group">
    <label for="tipo">Fotografía</label>
    <img src="{{asset('storage/actividades/'.$actividad->nombreImagen)}} " width="100px" height="100px" >
    <input type="file" class="form-control-file" name="fotografia" >
  </div>
  <button type="submit" class="btn btn-success">Editar</button>
  <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#exampleModal">Eliminar</button>

</form>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Eliminar actividad</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        ¿Desea eliminar la actividad?
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
        <a class="btn btn-danger" href="{{route('/actividades/eliminar', ['id' => $actividad->id])}}">Eliminar</a>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
  $('#addDate').on('click', function(e){
    e.preventDefault();
    var fecha = '<div class="input-group"><div class="col"><label for="fecha">Fecha</label><input type="text" class="form-control datepicker" name="datepicker[]"></div><div class="col"><label for="duracion">Duración</label><input type="time" class="form-control" name="duracion[]" min="00:01"></div><div class="col"></div></div>';
    $('#date-form').append(fecha);
  });

  $('#agregarPonente').on('click',function(e){
    e.preventDefault();

    var ponente=' <div class="form-group"><label for="ponente" >Ponente</label><div class="row"><div class=" col-md-6"><select class="custom-select" name="ponentes[]"><option value="" >--Seleccione ponente--</option>@foreach($ponentes->all() as $ponente)<option value="{{$ponente->id}}" >{{$ponente->nombre." ".$ponente->apellidoPaterno." ".$ponente->apellidoMaterno}}</option>@endforeach</select></div><div class="col-md-6 input-group"><select class="custom-select" name="roles[]"><option value="" >--Seleccione rol--</option><option value="Tallerista" >Tallerista</option><option value="Conferencista" >Conferencista</option><option value="Moderador" >Moderador</option><option value="Panelista" >Panelista</option></select><a class="btn btn-danger" href="{{route("/actividad/eliminarPonente",["id_actividad_ponente"=>$rol->id, "id_actividad"=>$actividad->id])}}">Eliminar</a></div></div></div>';
    $('#ponentes').append(ponente);
  });
</script>

@endsection