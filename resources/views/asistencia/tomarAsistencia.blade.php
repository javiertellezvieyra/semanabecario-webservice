@extends('layouts.admin')

@section('content')

<h1>Tomar asistencia</h1>
@if(!empty($registrados) )
  <div class="alert alert-warning alert-block">
    <button type="button" class="close" data-dismiss="alert">×</button>
    <strong>Los siguientes códigos ya estan registrados:</strong>
    <ul>
      @foreach($registrados as $r)
        <li>{{$r}}</li>
      @endforeach
    </ul>
  </div>
@endif
@if(!empty($fracasos))
  <div class="alert alert-danger alert-block">
    <button type="button" class="close" data-dismiss="alert">×</button>
    <strong>Los siguientes códigos no pudieron ser registrados:</strong>
    <ul>
      @foreach($fracasos as $r)
        <li>{{$r}}</li>
      @endforeach
    </ul>
   </div>
@endif
@if(!empty($exito))
  <div class="alert alert-success alert-block">
    <button type="button" class="close" data-dismiss="alert">×</button>
    <strong>La asistencia fue registrada exitosamente.</strong>
  </div>
@endif


@if($errors->any())
      <div class="alert alert-danger">
        <ul>
          @foreach($errors->all() as $error)
            <li>{{$error}}</li>
          @endforeach
        </ul>
      </div>
@endif

<form method="post" action="{{route('/asistencia/registrarAsistencias')}}" enctype="multipart/form-data">
  @csrf
  <div class="form-group" id="codigo">
    <input type="text" class="form-control" id="cogigo" name="codigos[]" placeholder="Código">
    <br>
  </div>
  <button class="btn btn-outline-secondary" id="agregarCodigo">+ Agregar código</button>
  <br>
  <br>
  <button type="submit" class="btn btn-success">Registrar</button>
</form>

<script type="text/javascript">
  $('#agregarCodigo').on('click',function(e){
    e.preventDefault();

    var codigo='<div class="form-group" id="codigo"><input type="text" class="form-control" id="cogigo" name="codigos[]" placeholder="Código"></div>';
    $('#codigo').append(codigo);
  });

</script>
@endsection
