@extends('layouts.admin')

@section('content')
<h1>Ponentes</h1>

@if ($message = Session::get('success'))

<div class="alert alert-success alert-block">

  <button type="button" class="close" data-dismiss="alert">×</button>

  <strong>{{ $message }}</strong>

</div>
@endif

@if($errors->any())
<div class="alert alert-danger">
  <ul>
    @foreach($errors->all() as $error)
    <li>{{$error}}</li>
    @endforeach
  </ul>
</div>
@endif
<a type="submit" class="btn btn-success" href="{{route('altaPonente')}}">Crear ponente</a>
<br><br><br>
<div class="card-deck">
  @forelse($ponentes->all() as $ponente)
  <div class="mb-3">
  <div class="card" style="width: 18rem;">
    <img class="card-img-top" src="{{asset('storage/ponentes/'.$ponente->nombreImagen)}}" alt="Imagen del ponente">
    <div class="card-body">
      <h5 class="card-title">{{$ponente->nombre." ".$ponente->apellidoPaterno." ".$ponente->apellidoMaterno}}</h5>
      <a href="{{route('/ponente/modificarPonente', $ponente->id)}}" class="btn btn-primary">Ver Detalle</a>
    </div>
  </div>
</div>
  @empty
  <p>No hay ponentes.</p>
  @endforelse
</div>
@endsection