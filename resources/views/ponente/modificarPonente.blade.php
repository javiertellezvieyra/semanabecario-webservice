@extends('layouts.app')

@section('content')
<h1>Registro de un Ponente</h1>

<form method="post" action="{{action('PonenteController@actualizarPonente')}}" enctype="multipart/form-data">
   @csrf
   <input type="hidden" name="id" value="{{$ponente[0]->id}}">
  <div class="form-group">
    <label for="nombre">Nombre</label>
    <input type="text" class="form-control" id="nombre" name="nombre" value="{{$ponente[0]->nombre}}">
  </div>
  <div class="form-group">
    <label for="apellidoPaterno">Primer apellido</label>
    <input type="text" class="form-control" id="apellidoPaterno" name="apellidoPaterno" value="{{$ponente[0]->apellidoPaterno}}">
  </div>
  <div class="form-group">
    <label for="apellidoMaterno">Segundo apellido</label>
    <input type="text" class="form-control" id="apellidoMaterno" name="apellidoMaterno" value="{{$ponente[0]->apellidoMaterno}}">
  </div>
  <div class="form-group">
    <label for="tipo">Grado académico</label>
    <input type="text" class="form-control" id="gradoAcademico" name="gradoAcademico" value="{{$ponente[0]->gradoAcademico}}">
  </div>
  <div class="form-group">
    <label for="tipo">Semblanza</label>
    <textarea name="semblanza" id="semblanza" class="form-control" rows="3" style="resize:none" >{{$ponente[0]->semblanza}}</textarea>
  </div>
  <div class="form-group">
    <label for="tipo">Fotografía</label>
    <input type="file" class="form-control-file" name="fotografia" >
  </div>
  <button type="submit" class="btn btn-success">Modificar</button>
  <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#eliminarPonente">Eliminar</button>
</form>

<!-- Modal -->
<div class="modal fade" id="eliminarPonente" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Eliminar ponente</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        ¿Desea eliminar al ponente {{$ponente[0]->nombre}} {{$ponente[0]->apellidoPaterno}} {{$ponente[0]->apellidoMaterno}}?
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
        <a class="btn btn-danger" href="{{route('/ponente/eliminarPonente',$ponente[0]->id)}}">Eliminar</a>
      </div>
    </div>
  </div>
</div>

@endsection