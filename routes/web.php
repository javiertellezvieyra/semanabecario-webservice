<?php
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

/*********/
/* JSON */
/*********/
Route::get('/actividades', 'ActividadController@obtenerActividades');
Route::get('/actividades/detalle/{id_actividad}', 'ActividadController@detalleActividad');
Route::post('/actividades/inscribir','ActividadController@inscribirAsistente');
Route::post('/actividades/desinscribir','ActividadController@desinscribirAsistente');
Route::post('/actividades/verificarInscripcion','ActividadController@verificarInscripcion');

// Sedes
Route::get('/sedes', 'SedesController@obtenerSedes');
Route::get('/sedes/detalle/{id_sede}', 'SedesController@detalleSede');

/* Asistentes */
Route::post('/asistente/registrar', 'AsistenteController@registrarAsistente');
Route::get('/asistentes', 'AsistenteController@obtenerAsistentes');
Route::post('/asistentes/misActividades','AsistenteController@misActividades');
Route::post('/asistentes/misNotificaciones','AsistenteController@misNotificaciones');
/*Ponenete*/
Route::get ('/ponente/obtenerPonente/{ponente}','PonenteController@getPonente');
// Fin JSON

/* Facultades */
/***************/
/* Actividades */
/***************/
Route::get('/actividades/altaActividad', 'ActividadController@altaActividad')->name('/actividades/altaActividad')->middleware('auth');
Route::post('/actividades/registrar', 'ActividadController@registrarActividad')->name('/actividades/registrar')->middleware('auth');

/* Muestra la lista de actividades. */
Route::get('/actividades/verActividades', 'ActividadController@verActividades')->name('/actividades/verActividades')->middleware('auth');
/* Muestra el detalle de una actividad. */
Route::get('/actividades/verActividades/{id}', 'ActividadController@verActividad')->name('/actividades/verActividades/detalle')->middleware('auth');
/* Elimina una actividad. */
Route::get('/actividades/eliminar/{id}', 'ActividadController@eliminarActividad')->name('/actividades/eliminar')->middleware('auth');
/* Actualiza una actividad. */
Route::post('/actividades/editarActividad', 'ActividadController@editarActividad')->name('/actividades/editarActividad')->middleware('auth');
/*Eliminar una fecha de una actividad*/
Route::get('/actividad/eliminarFecha/{idFecha}/actividad/{idActividad}','ActividadController@eliminarFecha')->name('/actividad/eliminarfecha')->middleware('auth');
/*Eliminar un ponente de una actividad */
Route::get('/actividad/eliminarPonente/{id_actividad_ponente}/actividad/{id_actividad}','ActividadController@eliminarPonente')->name('/actividad/eliminarPonente')->middleware('auth');
/* Administrador */
Route::get('/adminPanel', 'HomeController@index')->name('/adminPanel')->middleware('auth');
Auth::routes();

/*Asistentes*/
//Listar asistentes
Route::get('/asistentes/verAsistentes','AsistenteController@verAsistentes')->name('/asistente/verAsistentes');
//Muestra un asistente
Route::get('/asistentes/detalleAsistente/{asistente}','AsistenteController@detalleAsistente')->name('/asistentes/detalle');


/*Ponente*/

//Muestra un listado de los ponentes
Route::get('/ponente/verPonentes','PonenteController@verPonentes')->name('/ponente/verPonentes')->middleware('auth');
//Muestra la vista con el formulario para dar de alta a un ponente
Route::get('/ponente/altaPonente','PonenteController@altaPonente')->name('altaPonente')->middleware('auth');
//Registrar un ponente en la bd
Route::post('/ponente/registrarPonente','PonenteController@registrarPonente')->middleware('auth');
//Muestra la vista con el formulario para editar un ponente
Route::get('/ponente/modificarPonente/{ponente}','PonenteController@modificarPonente')->name('/ponente/modificarPonente')->middleware('auth');
Route::post('/ponente/actualizarPonente','PonenteController@actualizarPonente')->middleware('auth');
/*Eliminar ponente*/
Route::get('/ponente/eliminarPonente/{id}','PonenteController@eliminarPonente')->name('/ponente/eliminarPonente')->middleware('auth');

/***************/
/* 	  Sedes    */
/***************/
Route::get('/sedes/verSedes', 'SedesController@verSedes')->name('/sedes/verSedes')->middleware('auth');
Route::get('/sedes/verSedes/{id}', 'SedesController@verSede')->name('/sedes/verSedes/detalle')->middleware('auth');
Route::post('/sedes/editarSede', 'SedesController@editarSede')->name('/sedes/editarSede')->middleware('auth');
Route::get('/sedes/eliminar/{id}', 'SedesController@eliminarSede')->name('/sedes/eliminar')->middleware('auth');
Route::get('/sedes/altaSede', 'SedesController@altaSede')->name('/sedes/altaSede')->middleware('auth');
Route::post('/sedes/registrar', 'SedesController@registrarSede')->name('/sedes/registrar')->middleware('auth');

/****************/
/*  Asistencia  */
/****************/
Route::post('/asistencia', 'AsistenteController@registrarAsistencia');
//vista para tomar asisistencia

Route::get('/asistencia/tomarAsistencia', 'AsistenteController@tomarAsistencia')->name('/asistencia/tomarAsistencia')->middleware('auth');
Route::post('/asistencia/registrarAsistencias', 'AsistenteController@registrarAsistencias')->name('/asistencia/registrarAsistencias')->middleware('auth');



